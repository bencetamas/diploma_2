import numpy as np
import cv2
import cv2.ml
import random

def track(img_prev,img_curr,grid_size=1):
	# Dense method
	pts_prev=[]
	for x in range(0,img_prev.shape[0],grid_size):
		for y in range(0,img_prev.shape[1],grid_size):
			if not img_prev[x,y]==0:
				pts_prev=pts_prev+[(y,x)]
	pts_prev=np.array(pts_prev).astype(np.float32)

	# Do the tracking with Lucas-Kanade
	pts_curr, status, _=cv2.calcOpticalFlowPyrLK(img_prev,img_curr,pts_prev, None)
	return zip(pts_prev, pts_curr)

def get_moving_point_count(img_prev,img_curr,grid_size=1,min_dist=0,max_dist=40):
	pts_mov=track(img_prev,img_curr,grid_size)

	img_tmp=img_prev.copy()
	img_tmp=cv2.merge([img_tmp]*3)
	moving_points=0
	max_area=0
	max_contour=None
	mask=np.zeros(img_prev.shape[0:2],dtype=np.uint8)
	for pt_p,pt_c in pts_mov:
		dist=np.sqrt((pt_p[0]-pt_c[0])*(pt_p[0]-pt_c[0])+(pt_p[1]-pt_c[1])*(pt_p[1]-pt_c[1]))
		if (dist <min_dist or dist > max_dist):
			continue
		pt_pt=(int(pt_p[0]),int(pt_p[1]))
		pt_ct=(int(pt_c[0]),int(pt_c[1]))
		cv2.arrowedLine(img_tmp,pt_pt,pt_ct,(0,0,255),tipLength=0.3)
		cv2.rectangle(mask,(pt_pt[0]-grid_size,pt_pt[1]-grid_size),(pt_pt[0]+grid_size,pt_pt[1]+grid_size),(255),cv2.FILLED)
		bin=np.copy(mask)
		_, contours, _ = cv2.findContours(bin,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
		for contour in contours:
			if cv2.contourArea(contour)>max_area:
				max_area=cv2.contourArea(contour)
				max_contour=contour
		mask=np.zeros(img_prev.shape[0:2],dtype=np.uint8)
		if max_area>0:
			cv2.drawContours(mask,[max_contour],-1,(255))
		moving_points=moving_points+1
	cv2.imshow("moving of best points",img_tmp)
	cv2.imshow("mask",mask)
	return (moving_points,max_area)

	
cap = cv2.VideoCapture('ergo2.mp4')
for _ in range(290):
	cap.read()
_,img1 = cap.read()  
_,img2 = cap.read()
img1=cv2.Canny(img1,100,200,3)
img2=cv2.Canny(img2,100,200,3)
max_loc=-1
max_val=0
frame=[]
for t in range(250):
	_,num=get_moving_point_count(img1,img2,2)
	print "Processing frame "+str(t)+":"+str(num)
	if num>max_val:
		max_val=num
		max_loc=t
	frame=frame+[img1]

	img1=img2
	for _ in range(0):
		cap.read()
	_,img2=cap.read()
	img2=cv2.Canny(img2,100,200,3)

	if cv2.waitKey(2) & 0xFF == ord('q'):
        	break

track(frame[max_loc-1],frame[max_loc],5)
cv2.waitKey(0)
