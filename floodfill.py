
import numpy as np
import cv2
import cv2.ml
import random

from collections import deque

mouseCoord=[]
def mouseCallback(event, x, y, flags, param):
	global mouseCoord
	if event == cv2.EVENT_LBUTTONDOWN:
		mouseCoord = [(y, x)]

def getFFStartPoint(img):
	global mouseCoord
	cv2.namedWindow("select point")
	cv2.setMouseCallback("select point", mouseCallback)	
	while (len(mouseCoord) < 1):
		cv2.imshow("select point",img)
		cv2.waitKey(100)
	pt=mouseCoord[0]
	mouseCoord=mouseCoord[1:]
	return pt

def getFFMask(img,sp,xGap=20,yGap=2,xThresh1=50,yThresh1=30,dbg=False,dbgW=1,seedColor=None):
	colorNotFar=None

	def valid_point(pt):
		return pt[0]>0 and pt[1] > 0 and pt[0] < img.shape[0] and pt[1] < img.shape[1]

	def getThreshold(dir):
		if dir[1]==0:
			return yThresh1
		else:
			return xThresh1

	def colorNotFar1(color1,color2,thresh):
		return (color1-thresh<color2 or color1 < thresh) and (color1>color2-thresh or color2 < thresh)

	def colorNotFar3(color1,color2,thresh):
		return (color1[0]-thresh<color2[0] or color1[0] < thresh) and (color1[0]>color2[0]-thresh or color2[0] < thresh) and\
			(color1[1]-thresh<color2[1] or color1[1] < thresh) and (color1[1]>color2[1]-thresh or color2[1] < thresh) and\
			(color1[2]-thresh<color2[2] or color1[2] < thresh) and (color1[2]>color2[2]-thresh or color2[2] < thresh)

	def tryFFinDir(mask,pts_done,pts_edge,curr_pt,dirs,thresh):
		for dir in dirs:
			ut_pt=(curr_pt[0]+dir[0],curr_pt[1]+dir[1])
			if valid_point(ut_pt) and not pts_done[ut_pt]:
				if colorNotFar(seedColor,img[ut_pt],thresh):
					if pts_edge is not None:
						pts_edge.append(ut_pt)
					else:
						pts_edge=deque([ut_pt])
					mask[ut_pt]=FILLED_VALUE
					if dbg:
						cv2.imshow("dbg",mask)
						cv2.waitKey(dbgW)
				pts_done[ut_pt]=True
		return (mask,pts_done,pts_edge)

	# Init variables
	if seedColor is None:
		seedColor=img[sp]
	if len(img.shape)==2:
		colorNotFar=colorNotFar1
	else:
		colorNotFar=colorNotFar3
	xDirs=[(0,x) for x in range (1,xGap)]+[(0,-x) for x in range (1,xGap)]
	yDirs=[(y,0) for y in range (1,yGap)]+[(-y,0) for y in range (1,yGap)]
	dirs=xDirs+yDirs
	FILLED_VALUE=255
	mask=np.zeros(img.shape[:2],dtype=np.uint8)
	mask[sp]=FILLED_VALUE
	# Start the floodfill
	pts_done=np.zeros(img.shape[:2],dtype=np.bool)
	pts_done[sp]=True
	pts_edge=deque([sp])
	while pts_edge is not None and len(pts_edge)>0:
		curr_pt=pts_edge.popleft()
		mask,pts_done,pts_edge=tryFFinDir(mask,pts_done,pts_edge,curr_pt,xDirs,xThresh1)
		mask,pts_done,pts_edge=tryFFinDir(mask,pts_done,pts_edge,curr_pt,yDirs,yThresh1)
	return mask

# Returns a set of colors, which appears in the maskedregion of the image
def getColorsFromMaskedRegion(img,mask):
	colors=set([])
	for x in range(mask.shape[1]):
		for y in range(mask.shape[0]):
			if mask[y,x]!=0:
				rgb=img[y,x]
				rgb1=(rgb[0],rgb[1],rgb[2])
				colors.add(rgb1)
	return colors

# Create a binary mask, which mask out colors, that aren't in the 'colors' list
def createColorBasedMask(img,colors):
	mask=np.zeros(img.shape[:2],dtype=np.uint8)
	for x in range(img.shape[1]):
		for y in range(img.shape[0]):
			rgb=img[y,x]
			rgb1=(rgb[0],rgb[1],rgb[2])
			if rgb1 in colors:	
				mask[y,x]=255
	return mask

def main():
	cap=cv2.VideoCapture("/home/tamasbence/Documents/diploma_1/test_video2/rowing/test9.mp4")
	cap=cv2.VideoCapture("/home/tamasbence/Documents/diploma_1/test_video4/IMG_0658.MOV")
	cap=cv2.VideoCapture("test1.MPG")
	_,img1=cap.read()
	#img1=cv2.cvtColor(img1,cv2.COLOR_BGR2HSV)
	sp=getFFStartPoint(img1)
	#img1,_,_ = cv2.split(img1)
	mask=getFFMask(img1,sp)
	seedColor=img1[sp]
	desEx = cv2.xfeatures2d.SIFT_create()
	kp1=desEx.detect(img1,mask)
	kp1,des1=desEx.compute(img1,kp1)

	random.seed()
	#rm=cv2.resize(mask,(mask.shape[1]/3,mask.shape[0]/3))
	'''
	cv2.imshow("mask",mask)
	while (True):
		x=random.randint(0,img1.shape[1]-1)
		y=random.randint(0,img1.shape[0]-1)
		while mask[y,x]!=255:
			x=random.randint(0,img1.shape[1]-1)
			y=random.randint(0,img1.shape[0]-1)
		newMask=getFFMask(img1,(y,x))
		print "Showing new mask..."
		newMask=cv2.cvtColor(newMask,cv2.COLOR_GRAY2BGR)
		cv2.circle(newMask,(x,y),6,(0,0,255))
		#newMask2=cv2.resize(newMask,(mask.shape[1]/3,mask.shape[0]/3))
		cv2.imshow("new mask",newMask)
		if cv2.waitKey(0) & 0xFF=='q':
			break
	return
	'''
	goodColors=getColorsFromMaskedRegion(img1,mask)
	while (True):
		_,img1=cap.read()
		cv2.imshow("good colors1",createColorBasedMask(img1,goodColors))
		if cv2.waitKey(0) & 0xFF=='q':
			break
	return
	
	maskArea=sum(sum([m!=0 for m in mask]))
	print "Mask area="+str(maskArea)
	bRect=cv2.boundingRect(mask)
	maskROI=mask[bRect[1]:bRect[1]+bRect[3],bRect[0]:bRect[0]+bRect[2]]
	imgROI=img1[bRect[1]:bRect[1]+bRect[3],bRect[0]:bRect[0]+bRect[2]]
	imgTmp=img1.copy()
	cv2.imshow("desMask",maskROI)
	for i in range(len(kp1)):
		pt=(int(kp1[i].pt[0]),int(kp1[i].pt[1]))
		# Draw image
		cv2.circle(imgTmp,pt,4,(0,0,255))
		# Made floodfills
		print pt
		desMask=getFFMask(imgROI,(pt[1]-bRect[1],pt[1]-bRect[1]),seedColor=seedColor)
		cv2.imshow("desMask",desMask)
		cv2.waitKey(0)
		print "Descriptor and original mask overlap="+str(sum(sum([m!=0 for m in maskROI&desMask])))
	cv2.imshow("desc",imgTmp)
	cv2.imshow("mask",mask)
	cv2.waitKey(0)
	
main()	
