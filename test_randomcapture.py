import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
from utility import randomize_input as ri

cap = ri.RandomizedCapture('ergo1.mp4',0.01,0.4,True,0.001,0.05,4.0)
cap = ri.RandomizedCapture('ergo1.mp4')
cap.skip(289)

bg=cv2.imread("/home/tamasbence/Downloads/bg9.jpeg")
img1 = cap.read()*255.0
bg=cv2.resize(bg,(img1.shape[1],img1.shape[0]))
fourcc = cv2.VideoWriter_fourcc(*'MJPG')
#out = cv2.VideoWriter('BASIC13_WEB.avi',fourcc, 30.0, (img1.shape[1],img1.shape[0]))
for _ in range(1800):	
	img1 = cap.read()*255.0
	img1=img1.astype(np.uint8)
	mask1=cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
	_,mask1=cv2.threshold(mask1,180,255,cv2.THRESH_BINARY)
	#cv2.imshow("mask1",mask1);
	mask1=mask1/255.0
	mask1=cv2.merge([mask1]*3)
	img=bg*mask1+img1*(1.0-mask1)
	img=img.astype(np.uint8)
	cv2.imshow("img",img);
	#out.write(img)
	
	if cv2.waitKey(1) & 0xFF == ord('q'):
        	break

#out.release()
