import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
from utility import randomize_input as ri

cap = ri.RandomizedCapture('ergo1.mp4',0.01,0.4,True,0.001,0.05,4.0)
cap = ri.RandomizedCapture('ergo2.mp4')
#cap=ri.RandomizedCapture('/home/tamasbence/Documents/video_ergo/clips/full_body/BASIC1_FRONT.avi')
cap.skip(289)

for _ in range(1800):	
	img1=cap.read()
	img1=(img1*255).astype(np.uint8)
	#img1=cv2.imread("/home/tamasbence/Downloads/rejtveny.png")
	edges=cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
	edges=cv2.Canny(img1,20,40,3)

	'''
	cv2.imshow("edges",edges)
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break
	'''
	
	'''
	lines = cv2.HoughLines(edges,1,np.pi/180,200)
	for [[rho,theta]] in lines:
	    print rho
	    print theta
	    a = np.cos(theta)
	    b = np.sin(theta)
	    x0 = a*rho
	    y0 = b*rho
	    x1 = int(x0 + 1000*(-b))
	    y1 = int(y0 + 1000*(a))
	    x2 = int(x0 - 1000*(-b))
	    y2 = int(y0 - 1000*(a))

	    cv2.line(img1,(x1,y1),(x2,y2),(0,0,255),2)
	'''
	'''
	lines = cv2.HoughLinesP(edges,1,1*np.pi/180,100,2,40)
	for [[x1,y1,x2,y2]] in lines:
   		cv2.line(img1,(x1,y1),(x2,y2),(0,255,0),2)
	'''
	gray=cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
	circles = cv2.HoughCircles(gray,cv2.HOUGH_GRADIENT,1,20,
                            param1=50,param2=40,minRadius=20,maxRadius=50)
	circles = np.uint16(np.around(circles))
	for i in circles[0,:]:
   		# draw the outer circle
    		cv2.circle(img1,(i[0],i[1]),i[2],(0,255,0),2)
    		# draw the center of the circle
    		cv2.circle(img1,(i[0],i[1]),2,(0,0,255),3)
	cv2.imshow("blurred",img1)
	
	if cv2.waitKey(1) & 0xFF == ord('q'):
        	break

