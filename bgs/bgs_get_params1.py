import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

threshold=40
time_diff_small=20
time_diff_large=400

def diff_mask(img1,img2,threshold=threshold):
	diff=img1+(-1.0)*img2
	_,mask=cv2.threshold(diff,threshold,1.0,cv2.THRESH_BINARY)
	b,g,r=cv2.split(mask)
	mask_gray=cv2.bitwise_or(cv2.bitwise_or(b,g),r)
	return mask_gray

def read_and_filter(cap):
	_,img=cap.read()
	#img=cv2.GaussianBlur(img,(9,9),0)
	return img

cap = cv2.VideoCapture('../ergo2.mp4')
for i in range(290):
	cap.read()
while (True):
	img1=read_and_filter(cap)
	for i in range(5):
		cap.read()
	img2=read_and_filter(cap)

	#m=cv2.estimateRigidTransform(img1,img2,0)
	#img2=cv2.warpAffine(img2,m,(img1.shape[1],img1.shape[0]),cv2.INTER_NEAREST|cv2.WARP_INVERSE_MAP) 
	cv2.imshow("transformed",(img2+(-1.0)*img1)/255.0/2.0+0.5)

	mask=cv2.bitwise_or(diff_mask(img1,img2),diff_mask(img2,img1))
	kernel = np.ones((19,19),np.uint8)
	closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

	cv2.imshow("mask",mask)
	
	print "Size: "+str(img1.shape[0])+"x"+str(img1.shape[1])+"  ="+str(img1.shape[0]*img1.shape[1])
	print "Mask size:"+str(np.count_nonzero(mask))
	print "Ratio:"+str(np.count_nonzero(mask)/1.0/img1.shape[0]/img1.shape[1])
	print "------------------------"

	if cv2.waitKey(0) & 0xFF == ord('q'):
	       	break

bg=bg.astype(dtype=np.uint8)
cv2.namedWindow("bg")
cv2.imshow("bg",bg)
cv2.namedWindow("mask")
cv2.imshow("mask",mask)
cv2.waitKey(0)


