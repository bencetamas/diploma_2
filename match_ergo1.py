

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

scale=1.0

def extend_with_kp(desc,kp,factor=0.0):
	result=[]
	for i in range(len(desc)):
		result=result+[np.concatenate((desc[i],[int(kp[i].pt[0])*factor,int(kp[i].pt[1])*factor]))]
	return np.array(result).astype(dtype=np.float32)

def dist(pt1, pt2):
	return np.sqrt((pt1[0]-pt2[0])*(pt1[0]-pt2[0])+(pt1[1]-pt2[1])*(pt1[1]-pt2[1]))

def matchImages(img1,img2):
	# Initiate detector
	desc = cv2.ORB_create()
	#desc = cv2.xfeatures2d.SIFT_create()
	#desc = cv2.BRISK_create()
	#orb = cv2.xfeatures2d.FREAK_create()

	kp1, des1 = desc.detectAndCompute(img1,None)
	kp2, des2 = desc.detectAndCompute(img2,None)
	des1=extend_with_kp(des1,kp1)
	des2=extend_with_kp(des2,kp2)
	
	#bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True) 
	bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=False)
	#matches = bf.match(des1,des2)
	matches = bf.knnMatch(des1,des2, k=2)
	good = []
	for m,n in matches:
    		if m.distance < 0.75*n.distance:
        		good.append(m)
	matches=good
	matches = sorted(matches, key = lambda x:x.distance)
	#matches = [match for match in matches if dist(kp1[match.queryIdx].pt,kp2[match.trainIdx].pt) > 3.0]
	draw_params = dict(matchColor = (0,255,0), # draw matches in green color
		           singlePointColor = None,
		           matchesMask = None, # draw only inliers
		           flags = 2)
	match_img = cv2.drawMatches(img1,kp1,img2,kp2,matches[:200], None, **draw_params)

	cv2.namedWindow("main")
	#match_img=cv2.resize(match_img,(0,0),fx=1.4*scale,fy=2.8*scale)
	match_img=cv2.resize(match_img,(0,0),fx=1.0*scale,fy=1.5*scale)
	cv2.imshow("main",match_img)

cap1 = cv2.VideoCapture('ergo1.mp4')
cap2 = cv2.VideoCapture('ergo2.mp4')
for _ in range(290):
	cap1.read()
for _ in range(295):
	cap2.read()
  
_,img1 = cap1.read()
img1=cv2.resize(img1,(0,0),fx=1.0/scale,fy=1.0/scale)
while (True):
	 
	_,img2 = cap2.read()
	img2=cv2.resize(img2,(0,0),fx=1.0/scale,fy=1.0/scale)
	matchImages(img1,img2)
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break

