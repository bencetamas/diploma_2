import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

threshold=10

def contains_color(colors,color):
	for c in colors:
		if (abs(c[0:3]-1.0*color) < threshold).all():
			c[3]=c[3]+1
			return True 
	return False

def most_frequent(colors):
	most_f=colors[0][0:3]
	freq=colors[0][3]
	for c in colors[1:]:
		if c[3]>freq:
			most_f=c[0:3]
	return most_f

cap = cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/clips/full_body/BASIC5_WEB.avi')
#for i in range(310):
#	cap.read()
_,img1=cap.read()
colors=[]
for x in range(img1.shape[0]):
	col=[]
	for y in range(img1.shape[1]):
		col=col+[[np.append(img1[x,y],[1])]]
	colors=colors+[col]

for t in range(20):
	print "Processing frame "+str(t)
	_,img1 = cap.read() 
	for _ in range(5):
		cap.read() 
	for x in range(img1.shape[0]):
		for y in range(img1.shape[1]):
			if not contains_color(colors[x][y],img1[x,y]):
				colors[x][y]=colors[x][y]+[np.append(img1[x,y],[1])]

bg=np.zeros(img1.shape,dtype=np.uint8)
for x in range(img1.shape[0]):
	for y in range(img1.shape[1]):
		bg[x,y]=most_frequent(colors[x][y])

cv2.imshow("bg",bg)
while (True):	
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break
