import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import argparse
from os import walk
from os import path
import os

def split_video(videof,outdir,fname,split_size,rewrite=False):
	fname=fname.split(".")[0]
	NSG=fname.find("NSG")!=-1
	cap = cv2.VideoCapture(videof)
	print "processing:   "+videof
	fourcc = cv2.VideoWriter_fourcc(*'H264')
	ret,outi=cap.read()
	i=0
	while ret:
		j=0
		outname=outdir+"/"+fname+"_"+str(i/split_size)
		if NSG:
			outname=outname+"_NSG"
		outname=outname+".mp4"
		if os.path.isfile(outname):
			print "Skipping "+outname
			while ret and j<split_size:
				i=i+1
				ret=cap.grab()
				j=j+1
		else:			
			print outname
			out = cv2.VideoWriter(outname,fourcc, 25.0, (outi.shape[1],outi.shape[0]))
			while ret and j<split_size:
				#print "Writing file "+videof+" frame "+str(i)+"..."
				out.write(outi)
				i=i+1
				ret,outi=cap.read()
				j=j+1
			out.release()

parser = argparse.ArgumentParser(description='Split videos.',epilog='For more info contact tamasbence92@gmail.com')
parser.add_argument('-i','--indir',help="...")
parser.add_argument('-o','--outdir',help="....")
parser.add_argument('-s','--splitsize',help="size of generated splits in frame",type=int)
parser.add_argument('-r','--rewrite',help="if output file exist replace it with new",type=bool)
parser.set_defaults(splitsize=30*20) # default is 20 seconds
parser.set_defaults(rewrite=False)
args=parser.parse_args()  

file_list=[]
for (d, _dirnames, files) in walk(args.indir):
	for f in files:
		sep=os.sep
		if d[-1:]==os.sep:
			sep=""
		df=d+sep+f
		file_list=file_list+[(df,f)]

file_list.sort()
for df,f in file_list:
	split_video(df,args.outdir,f,args.splitsize,args.rewrite)    
