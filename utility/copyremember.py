import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import argparse
from os import walk
from os import path
import os
import random
import shutil

parser = argparse.ArgumentParser(description='Split videos.',epilog='For more info contact tamasbence92@gmail.com')
parser.add_argument('-i','--indir',help="...")
parser.add_argument('-o','--outdir',help="....")
parser.add_argument('-n','--number',help="number of randomly choosed file to copy",type=int)
parser.add_argument('-p','--processed',help="the file, which contains the already copied files")
args=parser.parse_args()  

random.seed()

processed=[]
print args.processed
with open(args.processed,"r") as f:
	for line in f:
		processed=processed+[line]

i=0
for (d, _dirnames, files) in walk(args.indir):
	for f in files:
		if not f in processed and i<args.number and random.randint(0,10) < 5:
			sep=os.sep
			if d[-1:]==os.sep:
				sep=""
			inf=d+sep+f
			outf=args.outdir+f
			processed=processed+[f]
			shutil.copyfile(inf,outf)
			i=i+1

with open(args.processed,"w") as f:
	for line in processed:
		f.write(line+"\n")
