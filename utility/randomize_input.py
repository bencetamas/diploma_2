import numpy as np
import cv2
import cv2.ml
import random

class RandomizedCapture:
	def __init__(self, filename, light_change_frequency=0.0, light_change_amount=0.4,can_flip=False, dither_value=0.0,speed_change_frequency=0.0, max_speed_change=2.0):
		self.light_change_frequency=light_change_frequency
		self.light_change_amount=light_change_amount
		self.can_flip=can_flip
		self.dither_value=dither_value
		self.speed_change_frequency=speed_change_frequency
		self.max_speed_change=max_speed_change
		self.capture=cv2.VideoCapture(filename)

		random.seed()
		
		self.speed=1.0
		self.light=0.0
		self.frame_number=0
		self.last_frame=None
		self.last_frame_count=0
		self.flip=can_flip and random.random()<0.5

	def __change_params(self):
		if random.random()<self.light_change_frequency:
			self.light=(random.random()-0.5)*2*self.light_change_amount
		if random.random()<self.speed_change_frequency:
			if random.random()<0.5:
				self.speed=random.uniform(1.0,self.max_speed_change)
			else:
				self.speed=random.uniform(1.0/self.max_speed_change,1.0)

	def skip(self,nFrames=1):
		for _ in range(nFrames):
			self.capture.read()
			

	def read(self):
		self.frame_number=self.frame_number+1
		self.__change_params()
		if self.speed < 1 and self.last_frame_count<1.0/self.speed and not self.last_frame==None:
			self.last_frame_count=self.last_frame_count+1
			return self.last_frame
		elif self.speed > 1:	
			for _ in range(int(self.speed)-1):
				self.capture.read()

		_,frame=self.capture.read()
		frame=frame/255.0+[self.light,self.light,self.light]
		M=np.array([[1.0+random.uniform(-self.dither_value,self.dither_value),random.uniform(-self.dither_value,self.dither_value),random.uniform(-self.dither_value*20,self.dither_value*20)],[random.uniform(-self.dither_value,self.dither_value),1.0+random.uniform(-self.dither_value,self.dither_value),random.uniform(-self.dither_value*20,self.dither_value*20)]])
		if self.flip:
			M[0,0]=-M[0,0]
			M[0,2]=M[0,2]+frame.shape[1]
		frame=cv2.warpAffine(frame,M,(frame.shape[1],frame.shape[0]))
		self.last_frame=frame
		self.last_frame_count=0
		return frame
