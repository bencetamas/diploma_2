import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

threshold=5
time_diff_small=20
time_diff_large=400

def diff_mask(img1,img2,threshold=threshold):
	diff=img1+(-1.0)*img2
	_,mask=cv2.threshold(diff,threshold,1.0,cv2.THRESH_BINARY)
	b,g,r=cv2.split(mask)
	mask_gray=cv2.bitwise_or(cv2.bitwise_or(b,g),r)
	return mask_gray

def mix_image(img1,img2,mask):
	if len(img1.shape)==3 and len(mask.shape)<3:
		mask=cv2.merge([mask,mask,mask],3)
	return (img1*mask+img2*(1-mask)).astype(img1.dtype)

def refine_bg(frame1,frame1s,frame2,mask):
	mask1p=diff_mask(frame1,frame2)
	mask1n=diff_mask(frame2,frame1)
	mask2p=diff_mask(frame1s,frame2)
	mask2n=diff_mask(frame2,frame1s)

	maskn=cv2.bitwise_and(mask1n,mask2n)
	maskp=cv2.bitwise_and(mask1p,mask2p)
	coveragen=2*np.count_nonzero(maskn)*1.0/(np.count_nonzero(mask1n)+np.count_nonzero(mask2n))
	coveragep=2*np.count_nonzero(maskp)*1.0/(np.count_nonzero(mask1p)+np.count_nonzero(mask2p))
	print coveragen
	print coveragep

	if coveragep > coveragen:
		bg_mix_mask=cv2.bitwise_and(mask1p,mask2p)
		neg_mask=cv2.bitwise_or(mask1n,mask2n)
	else:
		bg_mix_mask=cv2.bitwise_and(mask1n,mask2n)
		neg_mask=cv2.bitwise_or(mask1p,mask2p)
	bg_mix_max=(1-neg_mask)*bg_mix_mask
	bg=mix_image(frame1,frame2,bg_mix_mask)	
	return (bg,bg_mix_mask)

cap = cv2.VideoCapture('ergo1.mp4')
for i in range(290):
	cap.read()
for i in range(50):
	cap.read()
frame=[]
while len(frame)<time_diff_large:
	_,tmp_img=cap.read()
	frame=frame+[tmp_img]

bg=frame[0]
mask=np.ones(frame[0].shape,dtype=np.float64)
for i in range(time_diff_large/3*2,time_diff_large-1,30):
	tmp_bg,tmp_mask=refine_bg(frame[0],frame[time_diff_small-1],frame[i],mask)
	tmp_mask=cv2.merge([tmp_mask,tmp_mask,tmp_mask],3)
	r=np.ones([bg.shape[0]*2,bg.shape[1]*2,3])
	r[0:bg.shape[0],0:bg.shape[1]]=tmp_bg
	r[0:bg.shape[0],bg.shape[1]:2*bg.shape[1]]=bg/255
	r[bg.shape[0]:2*bg.shape[0],0:bg.shape[1]]=tmp_mask
	r[bg.shape[0]:2*bg.shape[0],bg.shape[1]:2*bg.shape[1]]=mask
	cv2.imshow("r",r)
	cv2.imshow("frame0",frame[0])
	cv2.waitKey(0)
	
	bg=bg*(1-mask)+tmp_bg*(mask)
	mask=cv2.bitwise_and(mask,tmp_mask)

bg=bg.astype(dtype=np.uint8)
cv2.namedWindow("bg")
cv2.imshow("bg",bg)
cv2.namedWindow("mask")
cv2.imshow("mask",mask)
cv2.waitKey(0)

'''
cv2.namedWindow("i1")
cv2.imshow("i1",frame[0])
cv2.namedWindow("i2")
cv2.imshow("i2",frame[time_diff_small-1])
cv2.namedWindow("i3")
cv2.imshow("i3",frame[time_diff_large-1])
'''

'''
mask1p=diff_mask(frame[0],frame[time_diff_large-1])
cv2.namedWindow("mask1p")
cv2.imshow("mask1p",mask1p)
mask1n=diff_mask(frame[time_diff_large-1],frame[0])
cv2.namedWindow("mask1n")
cv2.imshow("mask1n",mask1n)
mask2p=diff_mask(frame[time_diff_small-1],frame[time_diff_large-1])
cv2.namedWindow("mask2p")
cv2.imshow("mask2p",mask2p)
mask2n=diff_mask(frame[time_diff_large-1],frame[time_diff_small-1])
cv2.namedWindow("mask2n")
cv2.imshow("mask2n",mask2n)

maskn=cv2.bitwise_and(mask1n,mask2n)
maskp=cv2.bitwise_and(mask1p,mask2p)
coveragen=2*np.count_nonzero(maskn)*1.0/(np.count_nonzero(mask1n)+np.count_nonzero(mask2n))
coveragep=2*np.count_nonzero(maskp)*1.0/(np.count_nonzero(mask1p)+np.count_nonzero(mask2p))
print coveragen
print coveragep

if coveragep > coveragen:
	bg_mix_mask=cv2.bitwise_or(mask1p,mask2p)
else:
	bg_mix_mask=cv2.bitwise_or(mask1n,mask2n)
bg=mix_image(frame[0],frame[time_diff_large-1],bg_mix_mask)
cv2.namedWindow("bg")
cv2.imshow("bg",bg)
cv2.waitKey(0)
'''

'''
_,img1 = cap.read()
img1=cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
colors=[img1*1.0]
counts=[np.ones(img1.shape,dtype=np.float32)]
while (True):	
	_,img1 = cap.read() 
	img1=cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
	subs=np.zeros(img1.shape,dtype=np.float32)
	mask=np.zeros(img1.shape,dtype=np.float32)
	for i in range(len(colors)):
		sub=colors[i]+(-1.0)*img1
		_,sub1=cv2.threshold(sub,threshold,255,cv2.THRESH_BINARY)
		_,sub2=cv2.threshold(-sub,threshold,255,cv2.THRESH_BINARY)
		#cv2.imshow('s1',sub1/255.0)
		#cv2.imshow('s2',sub2/255+sub1/255.0)
		sub=sub1+sub2
		_,sub=cv2.threshold(sub,1,255,cv2.THRESH_BINARY)
		sub=sub
		subs=subs+sub
		sub=(255-sub)/255
		counts[i]=counts[i]+sub*(1.0-mask)
		mask=mask+sub
		_,mask=cv2.threshold(mask,0.1,1,cv2.THRESH_BINARY)
	subs=subs/255
	_,subs=cv2.threshold(subs,len(colors)-1+0.1,1.0,cv2.THRESH_BINARY)
	if (subs > 0).any():
		cv2.imshow('c',img1*subs/255.0)
		colors=colors+[img1*1.0]
		counts=counts+[subs]
	cv2.imshow('frame',subs)
	print len(colors)
	
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break

ids=np.array(counts).argmax(axis=0)
result=np.zeros(img1.shape,dtype=np.float32)
for i in range(len(colors)):
	result=result+colors[i]*(ids==i).astype(int)
cv2.imshow("result",result/255);
if cv2.waitKey(0) & 0xFF == ord('q'):
       	pass
'''

