import numpy as np
import cv2
import json
import random

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((7*6,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)
print objp

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

for f in ["chessboard/sample/left"+'{0:02d}'.format(i+1)+".jpg" for i in range(13)]:
	print f
	img=cv2.imread(f)
	gray=img
	#cv2.imshow('imgorig',img)
	#cv2.waitKey(0)
	gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

	# Find the chess board corners
	ret, corners = cv2.findChessboardCorners(gray, (7,6),None)

	# If found, add object points, image points (after refining them)
	if ret == True:
		cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)

		# Draw and display the corners
		cv2.drawChessboardCorners(img, (7,6), corners,ret)
		cv2.imshow('img',img)
		ch=(cv2.waitKey(0) & 0xFF)
		if (ch == ord('n')):
			objpoints.append(objp.tolist())
			imgpoints.append(corners.tolist())
		elif (ch == ord('q')):
			break

objpoints=np.array(objpoints,dtype=np.float32)
imgpoints=np.array(imgpoints,dtype=np.float32)
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img.shape[0:2],None,None)
h,w =img.shape[0:2]
newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
# undistort
mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)

for f in ["chessboard/sample/left"+'{0:02d}'.format(i+1)+".jpg" for i in range(13)]:
	img=cv2.imread(f)
	dst = cv2.undistort(img, mtx, dist, None, newcameramtx)
	cv2.imshow("undistort",dst)
	cv2.waitKey(0)
