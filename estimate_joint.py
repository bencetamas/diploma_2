import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random
import json
import argparse

kernel1=np.ones((1,1),np.uint8)
kernel2=np.ones((3,3),np.uint8)
videoname='/home/tamasbence/Documents/video_ergo/raw/marker/ergo13_s1.mp4'
videoname='/home/tamasbence/Documents/video_ergo/raw/marker/ergo14_1.mp4'
videoname='/home/tamasbence/Documents/video_temp/IMG_1156.mp4'
videoname='/home/tamasbence/Documents/video_ergo/raw/marker/2016_11_04_BAD3.mp4'
resize=0.5

joints=[]
init_joints=[]

def estimateJoint(j1,j2,l1,l2):
	a=float(j1[0]-j2[0])
	b=float(j1[1]-j2[1])
	
	if a==0:
		return j1
	c=float(l2*l2-l1*l1+j1[0]*j1[0]-j2[0]*j2[0]+j1[1]*j1[1]-j2[1]*j2[1])
	d=c/2/a
	e=j2[0]-d

	# solving the quadratic equation
	ass=1+b*b/(a*a)
	bs=2*(e*b/a-j2[1])
	cs=e*e+j2[1]*j2[1]-l2*l2
      	if (ass==0):
        	r=(int(d-y*b/a),int(-cs/bs))
		return r
	else:
        	D=bs*bs-4*ass*cs
         	if (D < 0):
            		print "No solution!"
			return (int((j1[0]*l2+j2[0]*l1)/(l1+l2)),int((j1[1]*l2+j2[1]*l1)/(l1+l2)))
		D=np.sqrt(D);
        	D=-bs-D
        	D=D/2/ass
		print a
		print D
        	r=(int(d-D*b/a),int(D))
		return r

def resized(frame):
	global resize
	ret=cv2.resize(frame,dsize=(0,0),fx=resize,fy=resize)
	return ret

def mouseHandler1(event, x, y, flags, param):
	global resize
	x=int(x/resize)
	y=int(y/resize)
	global init_joints
	global joints
	if event == cv2.EVENT_LBUTTONDOWN:
		if len(init_joints) < 3:
			init_joints.append((x,y))
		elif len(joints)<2:
			joints.append((x,y))
		else:
			joints=[(x,y)]

def main():
	global init_joints,joints
	cap=cv2.VideoCapture(videoname)
	for i in range(45):
		cap.grab()
	_,frame=cap.read()

	cv2.namedWindow('setup1')
	cv2.setMouseCallback("setup1",mouseHandler1)
	while (len(init_joints) < 3):
		v=frame.copy()
		for p in init_joints:
			cv2.circle(v,p,6,(0,0,255))
		cv2.imshow("setup1",resized(v))
 		c=cv2.waitKey(50)

	dx=init_joints[0][0]-init_joints[2][0]
	dy=init_joints[0][1]-init_joints[2][1]
	l1=np.sqrt(dx*dx+dy*dy)
	dx=init_joints[1][0]-init_joints[2][0]
	dy=init_joints[1][1]-init_joints[2][1]
	l2=np.sqrt(dx*dx+dy*dy)
	print l1
	print l2

	while (True):
		v=frame.copy()
		for p in joints:
			cv2.circle(v,p,6,(0,0,255))
		if len(joints)==2:
			cv2.circle(v,estimateJoint(joints[0],joints[1],l1,l2),10,(255,0,128))
		cv2.imshow("setup1",resized(v))
		if c & 0xFF == ord('q'):
        		break
		elif c & 0xFF == ord('n'):
                	_,frame=cap.read()
		c=cv2.waitKey(50)

		
main()
