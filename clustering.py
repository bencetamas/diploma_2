
import numpy as np
import cv2
import cv2.ml
import random
import matplotlib.pyplot as plt

cap = cv2.VideoCapture('test1.MPG')
while (True):
	ret1,img1 = cap.read()  

	# Initiate detector
	orb = cv2.ORB_create()
	orb = cv2.xfeatures2d.SIFT_create()
	#orb = cv2.BRISK_create()

	kp1=orb.detect(img1,None)
	_,des1=orb.compute(img1,kp1)
	des1=des1.astype(dtype=np.float32)

	# Kmeans
	clusters=4
	criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
	compactness,labels,centers = cv2.kmeans(des1,clusters,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)

	# Draw result
	random.seed()
	resImg=img1.copy()
	for i in range(clusters):
		color=(random.randint(0,255),random.randint(0,255),random.randint(0,255))
		kpClus=np.compress(labels.ravel()==i,kp1)
		desClus=np.compress(labels.ravel()==i,des1)
		for kpTmp in kpClus:
			pt=(int(kpTmp.pt[0]),int(kpTmp.pt[1]))
			cv2.circle(resImg,pt,3,color,cv2.FILLED)
	cv2.imshow('result',resImg)
	cv2.waitKey(-1)
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break
