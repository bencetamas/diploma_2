import numpy as np
import cv2
import json

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((9*6,3), np.float32)
objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

cap=cv2.VideoCapture("chessboard/IPHONE4S_REAR2.MOV")
ret, img=cap.read()
i=0
while ret:
	gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

	# Find the chess board corners
	ret, corners = cv2.findChessboardCorners(gray, (9,6),None)

	# If found, add object points, image points (after refining them)
	if ret == True:
		cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)

		# Draw and display the corners
		cv2.drawChessboardCorners(img, (9,6), corners,ret)
		rs=cv2.resize(img,(640,480))
		cv2.imshow('img',rs)
		ch=(cv2.waitKey(0) & 0xFF)
		#ch=ord('n')
		if (ch == ord('n')):
			objpoints.append(objp.tolist())
			imgpoints.append(corners.tolist())
			#cv2.imwrite("chessboard/IPHONE4S_REAR2/"+str(i)+".jpeg",img)
			i=i+1
		elif (ch == ord('q')):
			break

	for i in range(20):
		cap.grab()
	ret, img=cap.read()

cv2.destroyAllWindows()
with open("chessboard/IPHONE4S_REAR3/points.json","w") as fo:
	json.dump({"objpoints":objpoints, "imgpoints":imgpoints},fo)
