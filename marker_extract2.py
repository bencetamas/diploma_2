#TODO: 
#      SPEEDUP: LUT

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random
import json
import argparse

kernel1=np.ones((1,1),np.uint8)
kernel2=np.ones((3,3),np.uint8)
videoname='/home/tamasbence/Documents/video_ergo/raw/marker/ergo13_s1.mp4'
videoname='/home/tamasbence/Documents/video_ergo/raw/marker/ergo14_1.mp4'
videoname='/home/tamasbence/Documents/video_temp/IMG_1156.mp4'
videoname='/home/tamasbence/Documents/video_ergo/raw/marker/2016_11_11_GOOD1.mp4'
resize=0.3
back_predict_kernel_size=9

ergo_pixels=[]
tracked=[
   { "name": "bluebox", "predictMoveIfNoContour": False, "maxXMove": 10000, "maxYMove": 10000, "maxMove": 10000, "dilation_iterations": 1, "apply_bluebox": False }, # this is special
   { "name": "test1", "predictMoveIfNoContour": False, "maxXMove": 60, "maxYMove": 10, "maxMove": 70, "dilation_iterations":1, "apply_bluebox": True} 
] # only template!


current_colors=[]
current_substitutions=[]
current_rect=[]
current_frame=None
mouse_down=False
color1=(120,120,120)
color2=(140,140,140)
default_interval=20
update=False
color_mode='static' # 'static', 'ratio'

brush_size=1
current_point=(0,0)
click=False
down=False

def getBlueboxMask(img):
	global tracked
	if tracked[0]["name"] == "bluebox":
		return 1-maskWithColors(img,tracked[0]["colors"],1)
	else:
		return np.ones(img.shape[0:2],np.uint8)
	

def resized(frame):
	global resize
	ret=cv2.resize(frame,dsize=(0,0),fx=resize,fy=resize)
	return ret

def updateTrackbar():
	global color1, color2, default_interval, update
	update=True
	cv2.setTrackbarPos('R1','setup1',int(color1[2]))
	cv2.setTrackbarPos('G1','setup1',int(color1[1]))
	cv2.setTrackbarPos('B1','setup1',int(color1[0]))
	cv2.setTrackbarPos('R2','setup1',int(color2[2]))
	cv2.setTrackbarPos('G2','setup1',int(color2[1]))
	cv2.setTrackbarPos('B2','setup1',int(color2[0]))
	cv2.setTrackbarPos('interval','setup1',default_interval)
	update=False

def trackbarHandler(_):
	global color1, color2, default_interval,update, current_colors
	if not update:
		color1 = (cv2.getTrackbarPos('B1','setup1'),cv2.getTrackbarPos('G1','setup1'),cv2.getTrackbarPos('R1','setup1'))
		color2 = (cv2.getTrackbarPos('B2','setup1'),cv2.getTrackbarPos('G2','setup1'),cv2.getTrackbarPos('R2','setup1'))
		default_interval=cv2.getTrackbarPos('interval','setup1')
		updateTrackbar()
		if (current_colors!=[]):
			current_colors[len(current_colors)-1]=(current_colors[len(current_colors)-1][0],color1,color2)

def mouseHandler1(event, x, y, flags, param):
	global resize
	x=int(x/resize)
	y=int(y/resize)
	global color1, color2, img1, default_interval, current_colors, color_mode
	global defining_substitution, define_substitutions, current_substitutions
	color=current_frame[y,x]
	if event == cv2.EVENT_LBUTTONDOWN:
		if color_mode=='static':
			color1=(max(color[0]-default_interval,0),max(color[1]-default_interval,0),max(color[2]-default_interval,0))
			color2=(min(color[0]+default_interval,255),min(color[1]+default_interval,255),min(color[2]+default_interval,255))
			current_colors.append(['static',color1,color2])
		else:
			ratio_diff=1.0/default_interval
			color=1.0*color
			color1=(color[0]/color[1]-ratio_diff,color[0]/color[2]-ratio_diff,color[1]/color[2]-ratio_diff)
			color2=(color[0]/color[1]+ratio_diff,color[0]/color[2]+ratio_diff,color[1]/color[2]+ratio_diff)
			current_colors.append(['ratio',color1,color2])
	updateTrackbar()

def mouseHandler2(event, x, y, flags, param):
	global resize
	x=int(x/resize)
	y=int(y/resize)
	global current_rect, mouse_down

	if event == cv2.EVENT_LBUTTONDOWN:
		current_rect = [x, y]
		mouse_down = True

	elif (event == cv2.EVENT_MOUSEMOVE and (flags & cv2.EVENT_FLAG_LBUTTON) != 0) or event == cv2.EVENT_LBUTTONUP:
		if len(current_rect)==2:
			current_rect.append(x-current_rect[0])
			current_rect.append(y-current_rect[1])
		else:
			current_rect[2]=x-current_rect[0]
			current_rect[3]=y-current_rect[1]
	if event == cv2.EVENT_LBUTTONUP:
		mouse_down = False

def mouseHandler3(event, x, y, flags, param):
	global current_point
	global click
	global resize
	global down
		
	if event == cv2.EVENT_LBUTTONDOWN:
		down=True
	elif event == cv2.EVENT_LBUTTONUP:
		down=False
	if down:
		current_point=(int(x/resize),int(y/resize))
		click=True

def maskWithColors(img,colors,fg_value=255):
	thr=np.zeros(img.shape[:2],np.uint8)
	if len(colors)==0:
		return thr
	for (mode,c1,c2) in colors:
		if mode=='static':
			b,g,r=cv2.split(img)
			_,b1=cv2.threshold(b,c1[0],fg_value,cv2.THRESH_BINARY)
			_,b2=cv2.threshold(b,c2[0],fg_value,cv2.THRESH_BINARY_INV)
			b=cv2.bitwise_and(b1,b2)
			_,g1=cv2.threshold(g,c1[1],fg_value,cv2.THRESH_BINARY)
			_,g2=cv2.threshold(g,c2[1],fg_value,cv2.THRESH_BINARY_INV)
			g=cv2.bitwise_and(g1,g2)
			_,r1=cv2.threshold(r,c1[2],fg_value,cv2.THRESH_BINARY)
			_,r2=cv2.threshold(r,c2[2],fg_value,cv2.THRESH_BINARY_INV)
			r=cv2.bitwise_and(r1,r2)
			thr_tmp=cv2.bitwise_and(b,cv2.bitwise_and(g,r))
			thr=cv2.bitwise_or(thr,thr_tmp)
		else:
			img_=img.copy().astype(np.float32)
			b,g,r=cv2.split(img_)
			r1=b/(g+0.0001)
			r2=b/(r+0.0001)
			r3=g/(r+0.0001)
			_,b1=cv2.threshold(r1,c1[0],fg_value,cv2.THRESH_BINARY)
			_,b2=cv2.threshold(r1,c2[0],fg_value,cv2.THRESH_BINARY_INV)
			b=cv2.bitwise_and(b1,b2)
			#cv2.imshow("test1",b)
			#print "Debug:"+str(c1)+","+str(c2)
			_,g1=cv2.threshold(r2,c1[1],fg_value,cv2.THRESH_BINARY)
			_,g2=cv2.threshold(r2,c2[1],fg_value,cv2.THRESH_BINARY_INV)
			g=cv2.bitwise_and(b1,b2)
			_,rr1=cv2.threshold(r3,c1[2],fg_value,cv2.THRESH_BINARY)
			_,rr2=cv2.threshold(r3,c2[2],fg_value,cv2.THRESH_BINARY_INV)
			rr=cv2.bitwise_and(rr1,rr2)
			thr_tmp=cv2.bitwise_and(b,cv2.bitwise_and(g,rr))
			thr_tmp=thr_tmp.astype(np.uint8)
			thr=cv2.bitwise_or(thr,thr_tmp)
	return thr

def substitute(img,colors,substitutions,mask,exact_mask=False,apply_bluebox=False):
	if len(colors)==0:
		return img

	if exact_mask:
		l,t,w,h=cv2.boundingRect(mask)
		if w==0 or h==0:
			return img
		substitutionMask=np.zeros((1,1),np.uint8)
		img_=img
		mask_=mask
		while (len(np.nonzero(substitutionMask)[0]) == 0):
			img=img_[t:t+h,l:l+w]
			mask=mask_[t:t+h,l:l+w]

			kernel1=np.ones((3,3),np.uint8)
			if apply_bluebox:
				blueboxMask=getBlueboxMask(img)
				blueboxMask=cv2.erode(blueboxMask,kernel1)
				blueboxMask=cv2.cvtColor(blueboxMask,cv2.COLOR_GRAY2BGR)
			else:
				blueboxMask=np.ones(img.shape,np.uint8)

			substitutionMask=maskWithColors(img,substitutions,fg_value=1)
			substitutionMask=substitutionMask*(1-cv2.dilate(mask,kernel1))
			substitutionMask=cv2.cvtColor(substitutionMask,cv2.COLOR_GRAY2BGR)
			if (len(np.nonzero(substitutionMask)[0]) == 0):
				l=max(l+10,0)
				t=max(t+10,0)
				w=w+20
				if l+w>img_.shape[1]:
					w=img.shape[1]-l
				h=h+20
				if t+h>img_.shape[0]:
					h=img.shape[0]-t

		mask=cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)*blueboxMask
		mask2=mask
		growing=img.copy()*substitutionMask
		while (len(np.nonzero(mask2)[0]) > 0):
			mask2=mask2*(1-substitutionMask)
			growing=cv2.dilate(growing,kernel1)
			img=img*(1-mask2)+growing*mask2
			substitutionMask=cv2.dilate(substitutionMask,kernel1)
		gauss=cv2.GaussianBlur(img,(5,5),0)
		img_[t:t+h,l:l+w] = img*(1-mask)+gauss*mask
		return img_
	else: # Return only a white region on image
		mask=maskWithColors(img,colors)
		mask=cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
		return img*(1-mask/255)+mask

def initTrackeds(tracked):
	for tr in tracked:
		tr["colors"]=[]
		tr["substitutions"]=[]
		tr["do_substitution"]=False
		tr["rect"]=None
		tr["center_history"]=[]
		tr["prediction_type"]=[]
		tr["back_history"]=[]

def contains(pixels,x,y):
	for pixel in pixels:
		if pixel[0]==x and pixel[1]==y:
			return True
	return False

def setupTracking(tracked,setup_ergo_pixels=False):
	global current_colors, current_rect, mouse_down, current_frame, color_mode
	global current_substitutions, define_substitutions
	initTrackeds(tracked)
	for tr in tracked:
		current_colors=[]
		current_rect=[]
		current_substitutions=[]
		do_substitutions=False

		cap=cv2.VideoCapture(videoname)
		_,frame=cap.read()
		current_frame=frame

		print "Setup "+tr["name"]+" begin point!"
		cv2.namedWindow('setup1')
		cv2.setMouseCallback("setup1",mouseHandler2)
		while (current_rect==[]):
			cv2.imshow("setup1",resized(frame))
 			c=cv2.waitKey(50)
		tr["center_history"]=[(current_rect[0],current_rect[1])]
		tr["prediction_type"]=["manual"]
		mouse_down=False
		current_rect=[]

		print "Setup "+tr["name"]+" tracking colors!"
		cv2.namedWindow('setup2')
		cv2.createTrackbar('R1','setup1',0,255,trackbarHandler)
		cv2.createTrackbar('G1','setup1',0,255,trackbarHandler)
		cv2.createTrackbar('B1','setup1',0,255,trackbarHandler)
		cv2.createTrackbar('R2','setup1',0,255,trackbarHandler)
		cv2.createTrackbar('G2','setup1',0,255,trackbarHandler)
		cv2.createTrackbar('B2','setup1',0,255,trackbarHandler)
		cv2.createTrackbar('interval','setup1',0,100,trackbarHandler)
		updateTrackbar()
		cv2.setMouseCallback("setup1",mouseHandler1)
		cv2.setMouseCallback("setup2",mouseHandler1)
		while (True):
			current_frame=frame
			rect_mask=resized(np.ones(frame.shape[:2],np.uint8))
			cv2.imshow("setup1",substitute(resized(frame),current_colors,current_substitutions,rect_mask))
			cv2.imshow("setup2",resized(maskWithColors(frame,current_colors)))
 			c=cv2.waitKey(25)
			if c & 0xFF == ord('q'):
        			break
			elif c & 0xFF == ord('n'):
                		_,frame=cap.read()
			elif c & 0xFF == ord('c'):
                		current_colors=[]
			elif c & 0xFF == ord('z') and len(current_colors)>0:
                		current_colors=current_colors[:-1]
				current_substitutions=current_substitutions[:-1]
			elif c & 0xFF == ord('r'):
                		if color_mode=='static':
					color_mode='ratio'
					print "Switch to ratio color mode."
				else:
					color_mode='static'
					print "Switched to static color mode."
			elif c & 0xFF == ord('s'):
				do_substitutions=True
				tr["colors"]=current_colors
				current_colors=[]
				print "Define substitution colors!"
			elif c & 0xFF == ord('d'):
				tr["dilation_iterations"]=tr["dilation_iterations"]+1
				print "The iteration of dilations is now:"+str(tr["dilation_iterations"])
			elif c & 0xFF == ord('e'):
				tr["dilation_iterations"]=max(tr["dilation_iterations"]-1,0)
				print "The iteration of dilations is now:"+str(tr["dilation_iterations"])
		cv2.destroyWindow("setup2")

		if do_substitutions:
			tr["substitutions"]=current_colors
		else:
			tr["colors"]=current_colors
		tr["do_substitution"]=do_substitutions

		print "Setup "+tr["name"]+" tracking rect!"
		cv2.setMouseCallback("setup1",mouseHandler2)
		c=ord('z')
		while c & 0xFF == ord('z'):
			current_rect=[]
			while (mouse_down or len(current_rect) < 4):
				frame_rect=np.copy(frame)
				if len(current_rect)>2:
					cv2.rectangle(frame_rect,(current_rect[0],current_rect[1]),(current_rect[0]+current_rect[2],current_rect[1]+current_rect[3]),(0,0,255),4)
				cv2.imshow("setup1",resized(frame_rect))
	 			c=cv2.waitKey(25)
				if c & 0xFF == ord('q'):
					break
				elif c & 0xFF == ord('n'):
		        		_,frame=cap.read()
			print "Press any key to accept rect, press z to undo."
			c=cv2.waitKey(0)
			
		cv2.destroyWindow("setup1")

		tr["rect"]=current_rect
		tr["do_substitution"]=do_substitutions

	global ergo_pixels, click, brush_size, current_point
	if setup_ergo_pixels:
		print "Setup ergo shape."
		cap=cv2.VideoCapture(videoname)
		_,frame=cap.read()
		current_frame=frame
		mask=np.zeros(frame.shape,np.uint8)

		cv2.namedWindow('setup1')
		cv2.setMouseCallback("setup1",mouseHandler3)
		while (True):
			current_frame=frame
			cv2.imshow("setup1",resized(frame*(1-mask/255)+mask))
 			c=cv2.waitKey(25)
			if click:
				for x in range(current_point[0]-brush_size/2,current_point[0]+brush_size/2+1):
					for y in range(current_point[1]-brush_size/2,current_point[1]+brush_size/2+1):
						if (x >=0 and x < frame.shape[1] and y >= 0 and y < frame.shape[0]):
							mask[y,x,:]=255
							if not contains(ergo_pixels,x,y):
								ergo_pixels.append((x,y))
				click=False
			if c & 0xFF == ord('q'):
        			break
			elif c & 0xFF == ord('n'):
                		_,frame=cap.read()
			elif c & 0xFF == ord('z') and len(ergo_pixels)>0:
				for pixel in ergo_pixels[-brush_size*brush_size:]:
					mask[pixel[1],pixel[0],:]=0
                		ergo_pixels=ergo_pixels[:-brush_size*brush_size]
			elif c & 0xFF == ord('d'):
				brush_size=brush_size+1
				print "The brush size is now:"+str(brush_size)
			elif c & 0xFF == ord('e'):
				brush_size=brush_size-1
				print "The brush size is now:"+str(brush_size)
		cv2.destroyWindow("setup1")
		

def getErgoMask(shape,ergo_pixels,fg_value=255):
	mask=np.zeros(shape[0:2],np.uint8)
	for pixel in ergo_pixels:
		mask[pixel[1],pixel[0]]=fg_value
	return mask

def getImageDiff(img1,img2,only_pos=False,bgr=False,threshold=30,blend_color_spaces=True,apply_gauss=False):
	if apply_gauss:
		img1_=cv2.GaussianBlur(img1,(3,3),-1)
		img2_=cv2.GaussianBlur(img2,(3,3),-1)
	else:
		img1_=img1
		img2_=img2
	diff=img1_+(-1.0)*img2_
	_,mask1=cv2.threshold(diff,threshold,255,cv2.THRESH_BINARY)
	_,mask2=cv2.threshold(diff,-threshold,255,cv2.THRESH_BINARY_INV)
	#_,mask1=cv2.threshold(diff,0,255,cv2.THRESH_TOZERO)
	#_,mask2=cv2.threshold(diff,0,255,cv2.THRESH_TOZERO_INV)
	if only_pos:
		mask=mask1
	else:
		mask=((mask1+mask2)).astype(np.uint8)
	if blend_color_spaces:
		b,g,r=cv2.split(mask)
		mask=cv2.bitwise_or(b,cv2.bitwise_or(g,r))
	else:
		mask,_,_=cv2.split(mask)
	if bgr:
		mask=cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
	return mask

def fg(frame,bg,surefg_mask,blueboxRectmask,threshold=30):
	return cv2.bitwise_or(getImageDiff(frame,bg,threshold=threshold)*blueboxRectmask*getBlueboxMask(frame),surefg_mask)

def predict_back(fg_mask,hip_pos,shoulder_pos):
	global back_predict_kernel_size
	kernel=np.ones((back_predict_kernel_size,back_predict_kernel_size),np.uint8)
	fg_mask=cv2.dilate(fg_mask,kernel)
	fg_mask=cv2.erode(fg_mask,kernel)	
	hx=hip_pos[0]
	hy=hip_pos[1]
	sx=shoulder_pos[0]
	sy=shoulder_pos[1]
	deltax=float(hx-sx)/(hy-sy)
	currback=[]
	y=sy
	currx=sx+10
	while y<=hy:
		currx=currx+10
		if currx >= fg_mask.shape[1]:
			currx=fg_mask.shape[1]-1
		while currx >=0 and fg_mask[y,currx,0]>0:
			currx=currx-1
		currback.append((currx,y))
		y=y+1
	return currback

def trackVideo(tracked,videoname,out_video,out_maskvideo,bg_video):
	global ergo_pixels

	cap=cv2.VideoCapture(videoname)
	ret,frame=cap.read()
	ret,frame=cap.read()

	blueboxRectmask=np.ones(frame.shape[0:2],np.uint8)	
	if tracked[0]["name"]=="bluebox":
		blueboxRect=tracked[0]["rect"]
		blueboxRectmask=np.zeros(frame.shape[0:2],np.uint8)
		cv2.rectangle(blueboxRectmask,(blueboxRect[0],blueboxRect[1]),(blueboxRect[0]+blueboxRect[2],blueboxRect[1]+blueboxRect[3]),1,-1)

	if out_video:
		fourcc = cv2.VideoWriter_fourcc(*'MJPG')
		out = cv2.VideoWriter(out_video,fourcc, cap.get(cv2.CAP_PROP_FPS), (frame.shape[1],frame.shape[0]))
		outvis = cv2.VideoWriter("visulization"+out_video,fourcc, cap.get(cv2.CAP_PROP_FPS), (frame.shape[1],frame.shape[0]))
	if out_maskvideo:
		fourcc = cv2.VideoWriter_fourcc(*'MJPG')
		outmask = cv2.VideoWriter(out_maskvideo,fourcc, cap.get(cv2.CAP_PROP_FPS), (frame.shape[1],frame.shape[0]))
		ergo_mask=getErgoMask(frame.shape,ergo_pixels)
		bgcap=cv2.VideoCapture(bg_video)
		ret,bg=bgcap.read()
		if not ret:
			print "Error when opening backgound video file."

	hip_pos=None
	shoulder_pos=None
	while (ret):
		frame_copy=np.copy(frame)
		curr_back=[]
		for tr in tracked:
			if "static" in tr.keys():
				continue

			s=len(tr["center_history"])
			r=tr["rect"]
			subframe=frame[r[1]:r[1]+r[3],r[0]:r[0]+r[2]]
			mask=maskWithColors(subframe,tr["colors"])
			mask=cv2.erode(mask,kernel1)
			mask=cv2.dilate(mask,kernel2)
			#cv2.imshow("mask1",mask)
			_,contours,_=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
			#mask=np.zeros(mask.shape[:2],np.uint8)
			#for i in range(len(contours)):
			#	cv2.drawContours(mask,contours,i,255,-1)
			#cv2.imshow("mask2",mask)
			last_center=(tr["center_history"][s-1][0]-r[0],tr["center_history"][s-1][1]-r[1])
			
			#min_dist=1000000
			min_dist=0
			min_ct=None
			min_contout=None
			for c in contours:
				x1,y1,w1,h1=cv2.boundingRect(c)
				ct=(x1+w1/2,y1+h1/2)
				dx=ct[0]-last_center[0]
				dy=ct[1]-last_center[1]
				dist=np.sqrt(dx*dx+dy*dy)
				'''if dist < min_dist and ((dist < tr["maxMove"] and abs(dx) < tr["maxXMove"] and abs(dy) < tr["maxYMove"]) or tr["prediction_type"][s-1] not in ["manual","exact"]):
				'''
				if min_dist < cv2.contourArea(c):
					min_dist=cv2.contourArea(c)
					min_ct=ct
					min_contour=c
			if min_ct!=None:
				tr["center_history"].append(min_ct)
				tr["prediction_type"].append("exact")
				mask=np.zeros(mask.shape[:2],np.uint8)
				cv2.drawContours(mask,[min_contour],0,255,-1)
			else:
				print "WARNING: no suitable object found when tracking "+tr["name"]+" using estimation from previous frames!"
				if s>1 and tr["predictMoveIfNoContour"]:
					last_center2=(tr["center_history"][s-2][0]-r[0],tr["center_history"][s-2][1]-r[1])
					last_displ=(last_center[0]-last_center2[0],last_center[1]-last_center2[1])
					ct=(last_center[0]+last_displ[0],last_center[1]+last_displ[1])
					tr["center_history"].append(ct)
					tr["prediction_type"].append("estimation1")
				else:
					tr["center_history"].append(last_center)
					tr["prediction_type"].append("estimation2")
			tr["center_history"][s]=(tr["center_history"][s][0]+r[0],tr["center_history"][s][1]+r[1])

			for i in range(tr["dilation_iterations"]):
				mask=cv2.dilate(mask,kernel2)
			mask2=np.zeros(frame.shape[:2],np.uint8)
			mask2[r[1]:r[1]+r[3],r[0]:r[0]+r[2]]=mask/255
			if tr["do_substitution"]:
				frame_copy=substitute(frame_copy,tr["colors"],tr["substitutions"],mask2,True,tr["apply_bluebox"])
	
			if tr["name"]=="hip":
				hip_pos=tr["center_history"][s]
			elif tr["name"]=="shoulder":
				shoulder_pos=tr["center_history"][s]

		if out_video:
			out.write(frame_copy)
		if out_maskvideo:
			fgm=fg(frame,bg,ergo_mask,blueboxRectmask)
			fgm=cv2.cvtColor(fgm,cv2.COLOR_GRAY2BGR)
			curr_back=predict_back(fgm,hip_pos,shoulder_pos)
			cv2.imshow("fgmask",resized(fgm))
			outmask.write(fgm)
		for tr in tracked:
			p=tr["center_history"][-1]
			p=(p[0],p[1])
			cv2.circle(frame_copy,p,int(3/resize),(0,0,255),-1)
		if not curr_back==[]:
			tr["back_history"].append(curr_back)
			for pixel in curr_back:
				frame_copy[pixel[1],pixel[0],2]=255
				frame_copy[pixel[1],pixel[0]-1,2]=255
		cv2.imshow("tracking",resized(frame_copy))
		if out_video:
			outvis.write(frame_copy)
		c=cv2.waitKey(0)
 		if c & 0xFF == ord('q'):
        		break
		ret,frame=cap.read()
	if out_video:
		out.release()
	if out_maskvideo:
		outmask.release()


parser = argparse.ArgumentParser()
parser.add_argument("--out", help="output json file", default="markers.json")
parser.add_argument("--tracking_setup", help="json file containing tracking infos", default="tracking_setup.json")
parser.add_argument("--ergo_pixels", help="json file containing the sure foreground (ergometer) pixels")
parser.add_argument("--setup", help="setup tracking", action="store_true")
parser.add_argument("--csv", help="output to csv also", action="store_true")
parser.add_argument("--video", help="output video file")
parser.add_argument("--mask", help="output video file for foreground mask")
parser.add_argument("--bg", help="video containing the backgound image")
parser.add_argument("--points_to_be_tracked", help="json file describing the points that should be tracked", default="tracking_setup.json")
args = parser.parse_args()

with open(args.points_to_be_tracked,"r") as fi:
	tracked=json.load(fi)
if args.setup:
	setupTracking(tracked,args.mask)
	if args.tracking_setup:
		with open(args.tracking_setup,"w") as fo:
			json.dump(tracked,fo)
	if args.ergo_pixels:
		with open(args.ergo_pixels,"w") as fo:
			json.dump(ergo_pixels,fo)
else:
	with open(args.tracking_setup,"r") as fi:
		tracked=json.load(fi)
	if args.ergo_pixels:
		with open(args.ergo_pixels,"r") as fi:
			ergo_pixels=json.load(fi)
		
trackVideo(tracked,videoname,args.video,args.mask,args.bg)
with open(args.out,"w") as fo:
	json.dump(tracked,fo)

if args.csv:
	with open(args.out.split(".")[0]+str(".csv"),"w") as fo:
		for i in range(len(tracked[0]["center_history"])):
			s=str(i)+";"
			for tr in tracked:
				s=s+str(tr["center_history"][i][0])+";"+str(tr["center_history"][i][1])+";"
			for tr in tracked:
				s=s+tr["prediction_type"][i]+";"
			print s
			fo.write(s+"\n")
