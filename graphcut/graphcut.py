

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random

video_start=1290
#video_start=31*30#30#22*24
video_name='../ergo1.mp4'
#video_name='ergo2.mp4'
#video_name='../ergo3.MOV'
#video_name="/home/tamasbence/Documents/video_ergo/raw/PETI/FRONT1.MOV"
#video_name="/home/tamasbence/Documents/video_ergo/raw/PETI/REAR3.MOV"
#video_name="/home/tamasbence/Documents/video_ergo/raw/PETI/youtube0.mp4"

cap = cv2.VideoCapture(video_name)
for i in range(video_start):
	cap.grab()
_,img=cap.read()
#img = cv2.imread('messi.jpeg')
mask=np.ones(img.shape[0:2],dtype=np.uint8)*100

rectPt1=None
rectPt2=None

def mouseHandler(event, x, y, flags, param):
	global mask
	global rectPt1,rectPt2
	if event == cv2.EVENT_LBUTTONDOWN:
		if rectPt1==None:
			rectPt1=(x,y)
		elif rectPt2==None:
			rectPt2=(x,y)
		else:
			cv2.circle(mask,(x,y),3,(255),-1)
	elif event == cv2.EVENT_RBUTTONDOWN:
		cv2.circle(mask,(x,y),3,(0),-1)

cv2.namedWindow('image')
cv2.namedWindow('mask')
cv2.setMouseCallback("image",mouseHandler)
cv2.setMouseCallback("mask",mouseHandler)

while (True):
	bgdModel = np.zeros((1,65),np.float64)
	fgdModel = np.zeros((1,65),np.float64)
 
	if rectPt2==None:
		seg=img
	else:
		m=np.zeros(img.shape[:2],dtype=np.uint8)
		rect=(rectPt1[0],rectPt1[1],rectPt2[0]-rectPt1[0],rectPt2[1]-rectPt1[1])
		cv2.grabCut(img,m,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)

		mask2 = np.where((m==2)|(m==0),0,1).astype('uint8')

		m[mask==255]=1
		m[mask==0]=0
		m, bgdModel, fgdModel = cv2.grabCut(img,m,None,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)

		mask2 = np.where((m==2)|(m==0),0,1).astype('uint8')
		seg = img*mask2[:,:,np.newaxis]

	cv2.imshow("image",seg)
	cv2.imshow('mask',mask)
	
	c=cv2.waitKey(25)
	if c & 0xFF == ord('q'):
        	break
	elif c & 0xFF == ord('n'):
		_,img1 = cap.read()  
		img1=cv2.resize(img1,None,fx=0.5,fy=0.5)




