import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

threshold=8
map_dim=256/threshold

def most_frequent(colors):
	return colors[0,0,0]
	most_f=None
	freq=-1
	for b in range(map_dim):
		for g in range(map_dim):
			for r in range(map_dim):		
				if colors[b,g,r]>freq:
					freq=colors[b,g,r]
					most_f=(b,g,r)
	return most_f

cap = cv2.VideoCapture('ergo1.mp4')
for i in range(310):
	cap.read()
_,img1=cap.read()
colors=[]
for x in range(img1.shape[0]):
	col=[]
	for y in range(img1.shape[1]):
		field=np.zeros((map_dim,map_dim,map_dim),dtype=np.uint8)
		field[img1[x,y]/threshold]=1
		col=col+[field]
	colors=colors+[col]

print "Scanning images...."
for t in range(2):
	print "Processing frame "+str(t)
	_,img1 = cap.read() 
	for _ in range(5):
		cap.read() 
	for x in range(img1.shape[0]):
		for y in range(img1.shape[1]):
			color=img1[x,y]/threshold
			field=colors[x][y][(color[0],color[1],color[2])]
			field=field+1

print "Creating background..."
bg=np.zeros(img1.shape,dtype=np.uint8)
for x in range(img1.shape[0]):
	print str(x)
	for y in range(img1.shape[1]):
		bg[x,y]=most_frequent(colors[x][y])

cv2.imshow("bg",bg)
while (True):	
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break
