

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random

img1=None

def getImageDiff(img1,img2,only_pos=False,bgr=True,threshold=20,blend_color_spaces=True,apply_gauss=False):
	if apply_gauss:
		img1_=cv2.GaussianBlur(img1,(3,3),-1)
		img2_=cv2.GaussianBlur(img2,(3,3),-1)
	else:
		img1_=img1
		img2_=img2
	diff=img1_+(-1.0)*img2_
	_,mask1=cv2.threshold(diff,threshold,255,cv2.THRESH_BINARY)
	_,mask2=cv2.threshold(diff,-threshold,255,cv2.THRESH_BINARY_INV)
	#_,mask1=cv2.threshold(diff,0,255,cv2.THRESH_TOZERO)
	#_,mask2=cv2.threshold(diff,0,255,cv2.THRESH_TOZERO_INV)
	if only_pos:
		mask=mask1
	else:
		mask=((mask1+mask2)).astype(np.uint8)
	if blend_color_spaces:
		b,g,r=cv2.split(mask)
		mask=cv2.bitwise_or(b,cv2.bitwise_or(g,r))
	else:
		mask,_,_=cv2.split(mask)
	if bgr:
		mask=cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
	return mask


cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/ATC_0001.MOV')
for _ in range(2*60*60+20*60):
	cap.grab()

_,img1 = cap.read()  
img1=cv2.resize(img1,None,fx=0.5,fy=0.5)
bg=cv2.imread("marker_bgs/ATC_0001.png")

while (True):
	cv2.imshow('orig',img1)

	diff=getImageDiff(bg,img1)

	cv2.imshow("diff",diff)
	
	c=cv2.waitKey(25)
	if c & 0xFF == ord('q'):
        	break
	elif c & 0xFF == ord('n'):
		_,img1 = cap.read()  
		img1=cv2.resize(img1,None,fx=0.5,fy=0.5)




