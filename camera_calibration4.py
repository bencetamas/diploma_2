import numpy as np
import cv2
import json
import random
import math

def vec_length(c1,c2):
	x=c1[0]-c2[0]
	y=c1[1]-c2[1]
	return np.sqrt(x*x+y*y)

def printCSV(table,header=[]):
	s=""
	for h in header:
		if len(s)>0:
			s=s+";"
		s=s+h
	print s
	for row in table:
		s=""
		for c in row:
			if len(s)>0:
				s=s+";"
			s=s+str(c)
		print s

def calcDeg(coords):
	c1=coords[0,0]
	c2=coords[1,0]
	c3=coords[2,0]
	scalar=(c1-c2).dot(c3-c2)
	return np.arccos(scalar/vec_length(c1,c2)/vec_length(c2,c3))*180.0/3.14

mtx=None
dist=None
with open("chessboard/IPHONE4S_REAR3/mtx_dist_best.json","r") as fi:
		t=json.load(fi)
		t1=np.array(t["mtx"],dtype=np.float32)
		t2=np.array(t["dist"],dtype=np.float32)
		mtx=t1
		dist=t2

rvec=np.array([0.0, 0.0, 0.0],dtype=np.float32)
tvec=np.array([0.0, 0.0, 0.0],dtype=np.float32)

Qs=np.array([[100,0,200],[50,0,200],[0,75,200]],dtype=np.float32)

print "*** NO COORDINATE TRANSFORM ***"
print "Project with no distortion:"
qs,_ =cv2.projectPoints(Qs,rvec,tvec,mtx,np.array([0,0,0,0,0],dtype=np.float32))
for i in range(Qs.shape[0]):
	print "Case "+str(i)+": "+str(Qs[i])+" => "+str(qs[i])

print
print "Calc with matrix operations:"
for i in range(Qs.shape[0]):
	rmat,_=cv2.Rodrigues(rvec)
	W=np.zeros((3,4),dtype=np.float32)
	W[:,0:3]=rmat
	W[:,3]=tvec
	hom=np.zeros((4),dtype=np.float32)
	hom[0:3]=Qs[i]
	hom[3]=1.0
	result=mtx.dot(W).dot(hom)
	result=result/result[2]
	print "Case "+str(i)+": "+str(Qs[i])+" => "+str(result)

# ----------------------------------------------------------------------------
print
print "Project with distortion:"
qs,_ =cv2.projectPoints(Qs,rvec,tvec,mtx,dist)
for i in range(Qs.shape[0]):
	print "Case "+str(i)+": "+str(Qs[i])+" => "+str(qs[i])

print
print "Calc with matrix operations:"
for i in range(Qs.shape[0]):
	rmat,_=cv2.Rodrigues(rvec)
	W=np.zeros((3,4),dtype=np.float32)
	W[:,0:3]=rmat
	W[:,3]=tvec
	hom=np.zeros((4),dtype=np.float32)
	hom[0:3]=Qs[i]
	hom[3]=1.0
	result=W.dot(hom)
	result=result/result[2]
	x=result[0]
	y=result[1]
	dist_=dist[0]
	#r2=(x-mtx[0,2])*(x-mtx[0,2])+(y-mtx[1,2])*(y-mtx[1,2])
	r2=x*x+y*y
	x=(1+dist_[0]*r2+dist_[1]*r2*r2+dist_[4]*r2*r2*r2)*x+2*dist_[2]*x*y+dist_[3]*(r2+2*x*x)
	y=(1+dist_[0]*r2+dist_[1]*r2*r2+dist_[4]*r2*r2*r2)*y+2*dist_[3]*x*y+dist_[2]*(r2+2*y*y)
	x,y=mtx[0:2,:].dot(np.array([x,y,1]))
	print "Case "+str(i)+": "+str(Qs[i])+" => "+str([x,y])

print "*** COORDINATE TRANSFORM ***"
print "*** CENTER ROTATION ***"
rvec10deg=np.array([0.0, 3.14/2/10.0, 0.0],dtype=np.float32)
rvec0deg=np.array([0.0, 0.0, 0.0],dtype=np.float32)
tvec=np.array([0.0, 0.0, 4.0],dtype=np.float32)

Qs=np.array([[0.05,0.0,0.0],[-0.05,0.0,0.0]],dtype=np.float32)

print "Project with distortion 10 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec10deg,tvec,mtx,dist)
cl10=vec_length(qs[0,0],qs[1,0])
print "Projected length: "+str(cl10)+" px"

print "Project with distortion 0 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec0deg,tvec,mtx,dist)
cl0=vec_length(qs[0,0],qs[1,0])
qs,_ =cv2.projectPoints(Qs,rvec0deg,tvec,mtx,np.array([0,0,0,0,0],dtype=np.float32))
cl0nodist=vec_length(qs[0,0],qs[1,0])
print "Projected length: "+str(cl0)+" px"

print "Error percentage: "+str(abs((cl10-cl0)/cl0*100.0))

result=[]
for deg in np.arange(0.0,45.0,0.5):
	row=[deg]

	rvecxdeg=np.array([0.0, 3.14/180.0*deg, 0.0],dtype=np.float32)
	tvec=np.array([0.0, 0.0, 4.0],dtype=np.float32)

	qs,_ =cv2.projectPoints(Qs,rvecxdeg,tvec,mtx,dist)
	clx=vec_length(qs[0,0],qs[1,0])
	row.append(clx)
	row.append(abs((clx-cl0)/cl0*100.0))

	qs,_ =cv2.projectPoints(Qs,rvecxdeg,tvec,mtx,np.array([0,0,0,0,0],dtype=np.float32))
	clx=vec_length(qs[0,0],qs[1,0])
	row.append(clx)
	row.append(abs((clx-cl0nodist)/cl0nodist*100.0))
	result.append(row)
printCSV(result,["Degree","Projected length","Error %","Projected length without distortion","Error % no dist"])

print "*** EDGE PARALLEL ***"
rvec0deg=np.array([0.0, 0.0, 0.0],dtype=np.float32)
tvecE=np.array([1.9, 1.5, 4.0],dtype=np.float32)

Qs=np.array([[0.05,0.0,0.0],[-0.05,0.0,0.0]],dtype=np.float32)

print "Project with distortion parallel edge:"
qs,_ =cv2.projectPoints(Qs,rvec0deg,tvecE,mtx,dist)
print qs[0]
clE=vec_length(qs[0,0],qs[1,0])
print "Projected length: "+str(clE)+" px"

print "Error percentage: "+str(abs((clE-cl0)/cl0*100.0))

result=[]
for x in np.arange(0.0,2.0,0.05):
	row=[x,x*0.75]

	tvecxE=np.array([x, x*0.75, 4.0],dtype=np.float32)

	qs,_ =cv2.projectPoints(Qs,rvec0deg,tvecxE,mtx,dist)
	clx=vec_length(qs[0,0],qs[1,0])
	row.append(clx)
	row.append(abs((clx-cl0)/cl0*100.0))

	qs,_ =cv2.projectPoints(Qs,rvec0deg,tvecxE,mtx,np.array([0,0,0,0,0],dtype=np.float32))
	clx=vec_length(qs[0,0],qs[1,0])
	row.append(clx)
	row.append(abs((clx-cl0nodist)/cl0nodist*100.0))
	result.append(row)
printCSV(result,["X center","Y center","Projected length","Error %","Projected length without distortion","Error % no dist"])

print "*** EDGE 10 DEG ROTATION ***"
result=[]
for x in np.arange(0.0,2.0,0.05):
	row=[x,x*0.75]

	tvecxE=np.array([x, x*0.75, 4.0],dtype=np.float32)

	qs,_ =cv2.projectPoints(Qs,rvec10deg,tvecxE,mtx,dist)
	clx=vec_length(qs[0,0],qs[1,0])
	row.append(clx)
	row.append(abs((clx-cl0)/cl0*100.0))

	qs,_ =cv2.projectPoints(Qs,rvec10deg,tvecxE,mtx,np.array([0,0,0,0,0],dtype=np.float32))
	clx=vec_length(qs[0,0],qs[1,0])
	row.append(clx)
	row.append(abs((clx-cl0nodist)/cl0nodist*100.0))
	result.append(row)
printCSV(result,["X center","Y center","Projected length","Error %","Projected length without distortion","Error % no dist"])

print "*** 2 m LONG object center 0/10 deg rotation ***"
rvec10deg=np.array([0.0, 3.14/2/10.0, 0.0],dtype=np.float32)
rvec0deg=np.array([0.0, 0.0, 0.0],dtype=np.float32)
tvec=np.array([0.0, 0.0, 4.0],dtype=np.float32)

Qs=np.array([[1.0,0.0,0.0],[-1.0,0.0,0.0]],dtype=np.float32)

print "Project with distortion 10 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec10deg,tvec,mtx,dist)
cl10=vec_length(qs[0,0],qs[1,0])
print "Projected length: "+str(cl10)+" px"

print "Project with distortion 0 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec0deg,tvec,mtx,dist)
cl0=vec_length(qs[0,0],qs[1,0])
print "Projected length: "+str(cl0)+" px"

print "Error percentage: "+str(abs((cl10-cl0)/cl0*100.0))

print "Project with NO distortion 10 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec10deg,tvec,mtx,np.array([0,0,0,0,0],dtype=np.float32))
cl10=vec_length(qs[0,0],qs[1,0])
print "Projected length: "+str(cl10)+" px"

print "Project with NO distortion 0 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec0deg,tvec,mtx,np.array([0,0,0,0,0],dtype=np.float32))
cl0=vec_length(qs[0,0],qs[1,0])
print "Projected length: "+str(cl0)+" px"

print "Error percentage: "+str(abs((cl10-cl0)/cl0*100.0))
	
print "*** DEGREES ***"

rvec10deg=np.array([0.0, -3.14/2/10.0, 0.0],dtype=np.float32)
rvec0deg=np.array([0.0, 0.0, 0.0],dtype=np.float32)
tvec=np.array([0.0, 0.0, 4.0],dtype=np.float32)

Qs=np.array([[0.5,0.0,0.0],[0.0,0.0,0.0],[0.5,0.4,0.0]],dtype=np.float32)

print "Project with distortion 10 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec10deg,tvec,mtx,dist)
d10=calcDeg(qs)
print "Projected deg: "+str(d10)+" px"

print "Project with distortion 0 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec0deg,tvec,mtx,dist)
d0=calcDeg(qs)
print "Projected deg: "+str(d0)+" px"

print "Error percentage: "+str(abs((d10-d0)/d0*100.0))

print "Project with NO distortion 10 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec10deg,tvec,mtx,np.array([0,0,0,0,0],dtype=np.float32))
d10=calcDeg(qs)
print "Projected length: "+str(d10)+" px"

print "Project with NO distortion 0 deg center:"
qs,_ =cv2.projectPoints(Qs,rvec0deg,tvec,mtx,np.array([0,0,0,0,0],dtype=np.float32))
d0nodist=calcDeg(qs)
print "Projected length: "+str(d0)+" px"

print "Error percentage: "+str(abs((d10-d0nodist)/d0nodist*100.0))

Qs=np.array([[0.05,0.0,0.0],[0.0,0.0,0.0],[0.05,-0.04,0.0]],dtype=np.float32)
print "*** EDGE 10 DEG ROTATION DEGREE ***"
result=[]
for x in np.arange(0.0,2.0,0.05):
	row=[x,x*0.75]

	tvecxE=np.array([x, x*0.75, 4.0],dtype=np.float32)

	qs,_ =cv2.projectPoints(Qs,rvec10deg,tvecxE,mtx,dist)
	dx=calcDeg(qs)
	row.append(dx)
	row.append(abs((dx-d0)/d0*100.0))

	qs,_ =cv2.projectPoints(Qs,rvec10deg,tvecxE,mtx,np.array([0,0,0,0,0],dtype=np.float32))
	dx=calcDeg(qs)
	row.append(dx)
	row.append(abs((dx-d0nodist)/d0nodist*100.0))
	result.append(row)
printCSV(result,["X center","Y center","Projected deg","Error %","Projected deg without distortion","Error % no dist"])
