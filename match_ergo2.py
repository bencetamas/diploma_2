

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random

refPt=[]
cropping=False

def click_and_crop(event, x, y, flags, param):
	# grab references to the global variables
	global refPt, cropping
 
	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True

	elif event == cv2.EVENT_MOUSEMOVE and (flags & cv2.EVENT_FLAG_LBUTTON) != 0:
		if len(refPt)==1:
			refPt.append((x,y))
		else:
			refPt[1]=(x,y)
 
	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False

def add_simple_color_descriptors(img,kp,des,factor=1):
	return (kp,des)
	tmp=[]
	for i in range(len(kp)):
		pt=(int(kp[i].pt[1]),int(kp[i].pt[0]))
		colors1=img[pt[0],pt[1]]*factor
		roi2=img[pt[0]-1:pt[0]+1,pt[1]-1:pt[1]+1].flatten()
		colors2=(factor*np.average(roi2[0::3]),factor*np.average(roi2[1::3]),factor*np.average(roi2[2::3]))
		tmp=tmp+[np.concatenate((np.concatenate((des[i],colors1)),colors2))]
	return (kp,np.array(tmp))

def select_detect_compute(img,desEx):
	global refPt
	refPt=[]

	cv2.namedWindow("training")
	cv2.setMouseCallback("training", click_and_crop)	
	while (len(refPt) < 3):
        	withRectImg=img.copy()
		if len(refPt) >= 2:
			cv2.rectangle(withRectImg,refPt[0],refPt[1],(0,0,255))
		cv2.imshow('training',withRectImg)
		cv2.waitKey(100)

	img_selected=img[refPt[0][1]:refPt[2][1],refPt[0][0]:refPt[2][0] ]
	h, w, _=img.shape
	mask=np.zeros((h,w,1), dtype=np.uint8)
	cv2.rectangle(mask,refPt[0],refPt[2],(255),cv2.FILLED)
	invMask=255-mask
	
	#surf= cv2.xfeatures2d.SURF_create()
	posKp=desEx.detect(img,mask)
	_,posDes=desEx.compute(img,posKp)
	negKp=desEx.detect(img,invMask)
	_,negDes=desEx.compute(img,negKp)

	return (posKp, posDes, negKp, negDes)

cap = cv2.VideoCapture('ergo1.mp4')
#cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/clips/full_body/BASIC1_FRONT.avi')
cap2 = cv2.VideoCapture('ergo2.mp4')
for _ in range(290):
	cap.read()
	cap2.read()
ret1,img1 = cap.read()  
#img1=cv2.resize(img1,(0,0),fx=0.5,fy=1.0) 
 
# Initiate ORB detector
orb = cv2.ORB_create()
orb = cv2.xfeatures2d.SIFT_create()
orb = cv2.BRISK_create()
#orb = cv2.xfeatures2d.FREAK_create()
 
# find the keypoints and descriptors with ORB
trainingData=None
traininglabels=None
for _ in range(25):
	print refPt
	for _ in range(3):
		cap.read()
		cap2.read()
	if random.random()<0.5:
		_,img1=cap.read()
	else:
		_,img1=cap2.read()
	kp1, des1, negKp1, negDes1=select_detect_compute(img1,orb)
	_,des1=add_simple_color_descriptors(img1,kp1,des1)
	_,negDes1=add_simple_color_descriptors(img1,kp1,negDes1)

	td=np.concatenate((des1,negDes1))
	tl=np.concatenate((np.ones((len(des1)), dtype=np.int32),np.zeros((len(negDes1)), dtype=np.int32)))
	if trainingData==None:
		trainingData=td
		trainingLabels=tl
	else:
		trainingData=np.concatenate((td,trainingData))
		trainingData=trainingData.astype(np.float32)
		trainingLabels=np.concatenate((tl,trainingLabels))
# drawing keypoints
svm_img=img1.copy()
for kp in kp1:
	pt=(int(kp.pt[0]),int(kp.pt[1]))
	cv2.circle(svm_img,pt,6,(0,0,255),cv2.FILLED)
for kp in negKp1:
	pt=(int(kp.pt[0]),int(kp.pt[1]))
	cv2.circle(svm_img,pt,4,(255,0,0))
cv2.imshow('frame2',svm_img)

# SVM
svm = cv2.ml.SVM_create()
svm.setType(cv2.ml.SVM_C_SVC)
svm.setKernel(cv2.ml.SVM_POLY)
svm.setDegree(5)
svm.setGamma(1.0)
svm.setCoef0(0.0)
#svm.setC(10000000)
#svm.setNu(0.067)
# svm.setP(0.0)
# svm.setClassWeights(None)

svm= cv2.ml.KNearest_create()
svm.setDefaultK(5)

#svm.setTermCriteria((cv2.TERM_CRITERIA_COUNT, 100, 1.e-06))
svm.train(trainingData, cv2.ml.ROW_SAMPLE, trainingLabels)

cap = cv2.VideoCapture('ergo1.mp4')
cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/clips/full_body/BASIC1_FRONT.avi')
for _ in range(290):
	pass
	#cap.read()
while (True):
	ret2,img2 = cap.read() 
	#img2=cv2.resize(img2,(0,0),fx=0.5,fy=1.0) 
 	#surf= cv2.xfeatures2d.SURF_create()
        kp2=orb.detect(img2,None)
	_, des2 = orb.compute(img2,kp2)
	des2=des2.astype(np.float32)
	_,des2=add_simple_color_descriptors(img2,kp2,des2)
        des2=des2.astype(np.float32)
	labels2=svm.predict(des2)

	svm_img=img2.copy()
        for [label],kp in zip(labels2[1],kp2):
		pt=(int(kp.pt[0]),int(kp.pt[1]))
		if label==1:
			cv2.circle(svm_img,pt,3,(0,0,255),cv2.FILLED)
		else:
			cv2.circle(svm_img,pt,2,(255,0,0))
	cv2.imshow('frame',svm_img)
	
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break




