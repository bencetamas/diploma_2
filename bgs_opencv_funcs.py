import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

threshold=40

cap = cv2.VideoCapture('ergo1.mp4')
#cap = cv2.VideoCapture('ergo2.mp4')
cap = cv2.VideoCapture('ergo3.MOV')
#cap = cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/clips/full_body/BASIC1_FRONT.avi')
for i in range(1290):
	cap.grab()

def getMOG2():
	bgs=cv2.createBackgroundSubtractorMOG2(100,16)
	bgs.setDetectShadows(False)
	bgs.setNMixtures(5)
	print bgs.setBackgroundRatio(0.9)
	print bgs.getNMixtures()
	print bgs.getBackgroundRatio()
	print bgs.getVarThresholdGen()
	print bgs.getVarInit()
	print bgs.getComplexityReductionThreshold()
	return bgs

def getMOG():
	bgs=cv2.bgsegm.createBackgroundSubtractorMOG()
	bgs.setNoiseSigma(15)
	bgs.setHistory(15)
	print bgs.getNMixtures()
	print bgs.getBackgroundRatio()
	print bgs.getHistory()
	print bgs.getNoiseSigma()
	return bgs

def getGMG():
	bgs=cv2.bgsegm.createBackgroundSubtractorGMG()
	bgs.setDecisionThreshold(0.998)
	bgs.setMaxFeatures(128)
	print bgs.getNumFrames()
	print bgs.getDefaultLearningRate()
	print bgs.getDecisionThreshold()
	print bgs.getSmoothingRadius()
	print bgs.getMaxFeatures()
	return bgs

def getKNN():
	bgs=cv2.createBackgroundSubtractorKNN(detectShadows=False)
	bgs.setkNNSamples(4)
	bgs.setDist2Threshold(1300)
	print bgs.getHistory()
	print bgs.getkNNSamples()
	print bgs.getNSamples()
	print bgs.getDist2Threshold()
	return bgs

def graphCut(img,diff):
	kernel=np.ones((20,20),np.uint8)
	kernel2=np.ones((13,13),np.uint8)
	fg = cv2.erode(diff, kernel)	
	bg = cv2.dilate(diff, kernel2)
	mask=np.ones(img.shape[:2],np.uint8)*cv2.GC_PR_BGD
	mask[fg==255]=1
	mask[bg==0]=0
	
	cv2.imshow("graphcutmask",mask*50)
	
	bgdModel = np.zeros((1,65),np.float64)
	fgdModel = np.zeros((1,65),np.float64)
	if len(mask[mask==1])>0 and len(mask[mask==0])>0:
		mask, bgdModel, fgdModel = cv2.grabCut(img,mask,None,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)
	
		mask[mask==3]=255
		mask[mask==1]=255
		cv2.imshow("graphcut segmentation1",mask)

		img=np.copy(img)
		img[mask==0]=(0,0,0)
		img[mask==2]=(0,0,0)
		#img[mask==3]=(0,0,0)
		cv2.imshow("graphcut segmentation2",img)

bgs=getKNN()
while (True):	
	_,img1 = cap.read() 
	img1=cv2.GaussianBlur(img1,(9,9),0)
	fgmask=bgs.apply(img1)
	#kernel = np.ones((9,9),np.uint8)
	#fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_CLOSE, kernel)
	'''
	fcont=np.copy(fgmask)
	_, contours, _=cv2.findContours(fcont,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)
	result=np.zeros(fgmask.shape,dtype=np.uint8)
	points=[]
	for contour in contours:
		if cv2.arcLength(contour,True)>1550:
			points=points+contour.tolist()
	points=np.array(points)
	if len(points)>0:
		hull=cv2.approxPolyDP(points,0.01,True)#cv2.convexHull(points)
		cv2.drawContours(result,[hull],0,(255),2)
	#cv2.imshow("result",result)
	'''
	#cv2.imshow("fgmask",fgmask)
	graphCut(img1,fgmask)
	
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break

