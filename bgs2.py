import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

threshold=40

cap = cv2.VideoCapture('ergo1.mp4')
for i in range(290):
	cap.read()
frames=[]
_,img1 = cap.read()
img1=cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
colors=[img1*1.0]
counts=[np.ones(img1.shape,dtype=np.float32)]
while (True):	
	_,img1 = cap.read() 
	img1=cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
	subs=np.zeros(img1.shape,dtype=np.float32)
	mask=np.zeros(img1.shape,dtype=np.float32)
	for i in range(len(colors)):
		sub=colors[i]+(-1.0)*img1
		_,sub1=cv2.threshold(sub,threshold,255,cv2.THRESH_BINARY)
		_,sub2=cv2.threshold(-sub,threshold,255,cv2.THRESH_BINARY)
		#cv2.imshow('s1',sub1/255.0)
		#cv2.imshow('s2',sub2/255+sub1/255.0)
		sub=sub1+sub2
		_,sub=cv2.threshold(sub,1,255,cv2.THRESH_BINARY)
		sub=sub
		subs=subs+sub
		sub=(255-sub)/255
		counts[i]=counts[i]+sub*(1.0-mask)
		mask=mask+sub
		_,mask=cv2.threshold(mask,0.1,1,cv2.THRESH_BINARY)
	subs=subs/255
	_,subs=cv2.threshold(subs,len(colors)-1+0.1,1.0,cv2.THRESH_BINARY)
	if (subs > 0).any():
		cv2.imshow('c',img1*subs/255.0)
		colors=colors+[img1*1.0]
		counts=counts+[subs]
	cv2.imshow('frame',subs)
	print len(colors)
	
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break

ids=np.array(counts).argmax(axis=0)
result=np.zeros(img1.shape,dtype=np.float32)
for i in range(len(colors)):
	result=result+colors[i]*(ids==i).astype(int)
cv2.imshow("result",result/255);
if cv2.waitKey(0) & 0xFF == ord('q'):
       	pass
