

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random

ks1=10
ks2=10
ks3=10
ks4=10
morph1=0

def trackbarHandler(_):
	global ks1, ks2, ks3, ks4, morph1
	ks1 = cv2.getTrackbarPos('kernel1','image')
	ks2 = cv2.getTrackbarPos('kernel2','image')
	ks3 = cv2.getTrackbarPos('kernel3','image')
	ks4 = cv2.getTrackbarPos('kernel4','image')
	morph1 = cv2.getTrackbarPos('morph1','image')

img=cv2.imread("m6.png")
cv2.namedWindow('image')
cv2.createTrackbar('kernel1','image',1,60,trackbarHandler)
cv2.createTrackbar('kernel2','image',1,60,trackbarHandler)
cv2.createTrackbar('kernel3','image',1,60,trackbarHandler)
cv2.createTrackbar('kernel4','image',1,60,trackbarHandler)
cv2.createTrackbar('morph1','image',0,5,trackbarHandler)

while (True):
	kernel1=np.ones((ks1,ks1),np.uint8)
	kernel2=np.ones((ks1,ks1),np.uint8)
	kernel3=np.ones((ks1,ks1),np.uint8)
	kernel4=np.ones((ks1,ks1),np.uint8)
	if morph1==0:
		moimg=cv2.erode(img,kernel1)
		print "Erode"
	elif morph1==1:
		moimg=cv2.dilate(img,kernel1)
		print "Dilate"
	else:
		moimg=cv2.morphologyEx(img,morph1,kernel1)
		if morph1==2:
			print "Open"
		elif morph1==3:
			print "Close"
		elif morph1==4:
			print "Gradient"
		elif morph1==5:
			print "Top hat"
	moimg=cv2.cvtColor(moimg,cv2.COLOR_BGR2GRAY)
	dist=cv2.distanceTransform(moimg,cv2.DIST_L2,3)
	dist=dist*1.0/dist.max()


	cv2.imshow("image",img)
	cv2.imshow('morph',moimg)
	cv2.imshow('dist',dist)
	
	c=cv2.waitKey(500)
	if c & 0xFF == ord('q'):
        	break




