import numpy as np
import cv2
import json
import random

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

img=cv2.imread("chessboard/IPHONE4S_REAR/1.jpeg")

def calibrateRandom(view_num):
	objpoints=[]
	imgpoints=[]
	with open("chessboard/IPHONE4S_REAR3/points.json","r") as fi:
		t=json.load(fi)
		t1=np.array(t["objpoints"])*0.02 # Every cell was 2cm-> 0.02m
		t2=np.array(t["imgpoints"])
		for i in range(view_num):
			idx=random.randint(0,t1.shape[0]-1)
			objpoints.append(t1[idx])
			imgpoints.append(t2[idx])
	objpoints=np.array(objpoints).astype(np.float32)
	imgpoints=np.array(imgpoints).astype(np.float32)
	objpoints=np.array(t1).astype(np.float32)
	imgpoints=np.array(t2).astype(np.float32)

	ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img.shape[0:2],None,None)
	tot_error = 0
	for i in xrange(len(objpoints)):
		imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
		error = cv2.norm(imgpoints[i],imgpoints2, cv2.NORM_L2)/len(imgpoints2)
		tot_error += error

	print "mean error: ", tot_error/len(objpoints)
		
	return objpoints, imgpoints, tot_error

objpoints=[]
imgpoints=[]
tot_error=4000
for i in range(1):
	t1, t2, t3=calibrateRandom(16)
	ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(t1, t2, img.shape[0:2],None,None)
	with open("chessboard/IPHONE4S_REAR3/mtx_dist"+str(i)+".json","w") as fo:
		json.dump({'mtx':mtx.tolist(), 'dist': dist.tolist()},fo)
	if tot_error > t3:
		tot_error=t3
		objpoints=t1
		imgpoints=t2

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img.shape[0:2],None,None)
with open("chessboard/IPHONE4S_REAR3/mtx_dist_best.json","w") as fo:
		json.dump({'mtx':mtx.tolist(), 'dist': dist.tolist()},fo)
h,w =img.shape[0:2]
newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
# undistort
mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)


cap=cv2.VideoCapture("chessboard/IPHONE4S_REAR.MOV")
ret, img=cap.read()
while ret:
	dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)
	dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

	# crop the image
	x,y,w,h = roi
	#dst = dst[y:y+h, x:x+w]
	cv2.imshow('calibresult',dst)
	cv2.waitKey(0)

	ret, img=cap.read()
