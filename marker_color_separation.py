

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random

default_interval=20
cs=[]
color1=(120,120,120)
color2=(140,140,140)
img1=None
update=False

def updateTrackbar():
	global color1, color2, img1, default_interval, update
	update=True
	cv2.setTrackbarPos('R1','image',color1[2])
	cv2.setTrackbarPos('G1','image',color1[1])
	cv2.setTrackbarPos('B1','image',color1[0])
	cv2.setTrackbarPos('R2','image',color2[2])
	cv2.setTrackbarPos('G2','image',color2[1])
	cv2.setTrackbarPos('B2','image',color2[0])
	cv2.setTrackbarPos('interval','image',default_interval)
	update=False

def trackbarHandler(_):
	global color1, color2, img1, default_interval,update
	if not update:
		color1 = (cv2.getTrackbarPos('B1','image'),cv2.getTrackbarPos('G1','image'),cv2.getTrackbarPos('R1','image'))
		color2 = (cv2.getTrackbarPos('B2','image'),cv2.getTrackbarPos('G2','image'),cv2.getTrackbarPos('R2','image'))
		default_interval=cv2.getTrackbarPos('interval','image')
		updateTrackbar()
		if (cs!=[]):
			cs[len(cs)-1]=(color1,color2)

def mouseHandler(event, x, y, flags, param):
	global color1, color2, img1, default_interval, cs
	if event == cv2.EVENT_LBUTTONDOWN or event == cv2.EVENT_MBUTTONDOWN:
		if event == cv2.EVENT_LBUTTONDOWN:
			cs=[]
		color=img1[y,x]
		color1=(max(color[0]-default_interval,0),max(color[1]-default_interval,0),max(color[2]-default_interval,0))
		color2=(min(color[0]+default_interval,255),min(color[1]+default_interval,255),min(color[2]+default_interval,255))
	if event == cv2.EVENT_MBUTTONDOWN:
		cs.append((color1,color2))
	updateTrackbar()

cv2.namedWindow('image')
cv2.namedWindow('orig')
cv2.createTrackbar('R1','image',0,255,trackbarHandler)
cv2.createTrackbar('G1','image',0,255,trackbarHandler)
cv2.createTrackbar('B1','image',0,255,trackbarHandler)
cv2.createTrackbar('R2','image',0,255,trackbarHandler)
cv2.createTrackbar('G2','image',0,255,trackbarHandler)
cv2.createTrackbar('B2','image',0,255,trackbarHandler)
cv2.createTrackbar('interval','image',0,100,trackbarHandler)
updateTrackbar()
cv2.setMouseCallback("image",mouseHandler)
cv2.setMouseCallback("orig",mouseHandler)

#cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/ATC_0005.MOV')
#cap=cv2.VideoCapture('ergo3.MOV')
#cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/IMG_1114.MOV')
#cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/IMG_1131.MOV')
cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/ATC_0001.MOV')
#for _ in range(4500):
for _ in range(2*60*60+20*60):
	cap.grab()

_,img1 = cap.read()  
img1=cv2.resize(img1,None,fx=0.5,fy=0.5)

while (True):
	thr=np.zeros(img1.shape[:2],np.uint8)
	if (cs==[]):
		b,g,r=cv2.split(img1)
		_,b1=cv2.threshold(b,color1[0],255,cv2.THRESH_BINARY)
		_,b2=cv2.threshold(b,color2[0],255,cv2.THRESH_BINARY_INV)
		b=cv2.bitwise_and(b1,b2)
		_,g1=cv2.threshold(g,color1[1],255,cv2.THRESH_BINARY)
		_,g2=cv2.threshold(g,color2[1],255,cv2.THRESH_BINARY_INV)
		g=cv2.bitwise_and(g1,g2)
		_,r1=cv2.threshold(r,color1[2],255,cv2.THRESH_BINARY)
		_,r2=cv2.threshold(r,color2[2],255,cv2.THRESH_BINARY_INV)
		r=cv2.bitwise_and(r1,r2)
		thr=cv2.bitwise_and(b,cv2.bitwise_and(g,r))
	else:
		for (c1,c2) in cs:
			b,g,r=cv2.split(img1)
			_,b1=cv2.threshold(b,c1[0],255,cv2.THRESH_BINARY)
			_,b2=cv2.threshold(b,c2[0],255,cv2.THRESH_BINARY_INV)
			b=cv2.bitwise_and(b1,b2)
			_,g1=cv2.threshold(g,c1[1],255,cv2.THRESH_BINARY)
			_,g2=cv2.threshold(g,c2[1],255,cv2.THRESH_BINARY_INV)
			g=cv2.bitwise_and(g1,g2)
			_,r1=cv2.threshold(r,c1[2],255,cv2.THRESH_BINARY)
			_,r2=cv2.threshold(r,c2[2],255,cv2.THRESH_BINARY_INV)
			r=cv2.bitwise_and(r1,r2)
			thr_tmp=cv2.bitwise_and(b,cv2.bitwise_and(g,r))
			thr=cv2.bitwise_or(thr,thr_tmp)

	cv2.imshow("image",thr)
	cv2.imshow('orig',img1)
	
	c=cv2.waitKey(25)
	if c & 0xFF == ord('q'):
        	break
	elif c & 0xFF == ord('n'):
		#for i in range(5*60):
		#	cap.grab()  
                _,img1=cap.read()
		img1=cv2.resize(img1,None,fx=0.5,fy=0.5)




