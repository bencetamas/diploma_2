import numpy as np
import cv2
import cv2.ml
import random

refPt=[]
cropping=False

def click_and_crop(event, x, y, flags, param):
	# grab references to the global variables
	global refPt, cropping
 
	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True

	elif event == cv2.EVENT_MOUSEMOVE and (flags & cv2.EVENT_FLAG_LBUTTON) != 0:
		if len(refPt)==1:
			refPt.append((x,y))
		else:
			refPt[1]=(x,y)
 
	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False

def select(img):
	return [(0,0),(0,0)]
	global refPt
	refPt=[]

	cv2.namedWindow("training")
	cv2.setMouseCallback("training", click_and_crop)	
	while (len(refPt) < 3):
        	withRectImg=img.copy()
		if len(refPt) >= 2:
			cv2.rectangle(withRectImg,refPt[0],refPt[1],(0,0,255))
		cv2.imshow('training',withRectImg)
		cv2.waitKey(100)

	return (refPt[0],refPt[2])

def select_points(rect,grid_size=2):

	tl,br=rect
	pts=[]
	for x in range(tl[0],br[0],grid_size):
		for y in range(tl[1],br[1],grid_size):
			pts=pts+[(x,y)]
	return np.array(pts).astype(dtype=np.float32)

def track(img_prev,img_curr,rect_prev):
	rect_prev=((0,0),(img_prev.shape[1],img_prev.shape[0]))
	# Get the points to track
	#pts_prev=select_points(rect_prev)	
	# params for ShiTomasi corner detection
	mask=np.zeros(img_prev.shape[:2],dtype=np.uint8)
	img_gray=cv2.cvtColor(img_prev,cv2.COLOR_BGR2GRAY)
	rtl,rbr=rect_prev
	cv2.rectangle(mask,rtl,rbr,(255),cv2.FILLED)
	'''
	# Feature descriptor method
	des=cv2.xfeatures2d.SIFT_create()
	des=cv2.ORB_create()
	kps=des.detect(img_prev,mask)
	pts_prev=np.array([k.pt for k in kps]).astype(np.float32)
	'''
	'''
	# GoodFeaturesToTrack function
	pts_prev=np.array([(pt[0][0],pt[0][1]) for pt in cv2.goodFeaturesToTrack(img_gray,2000,0.01,0,mask=mask)]).astype(np.float32)
	'''
	'''
	# Edge based method
	pts_prev=[]
	dst=cv2.Canny(img_gray,100,200,3)
	for x in range(dst.shape[0]):
		for y in range(dst.shape[1]):
			if dst[x,y]==255:
				pts_prev=pts_prev+[(y,x)]
	pts_prev=np.array(pts_prev).astype(np.float32)
	'''
	# Dense method
	pts_prev=[]
	for x in range(0,img_prev.shape[0],5):
		for y in range(0,img_prev.shape[1],5):
			pts_prev=pts_prev+[(y,x)]
	pts_prev=np.array(pts_prev).astype(np.float32)

	# Do the tracking with Lucas-Kanade
	pts_curr, status, _=cv2.calcOpticalFlowPyrLK(img_prev,img_curr,pts_prev, None)
	pts_back, _, _=cv2.calcOpticalFlowPyrLK(img_curr,img_prev,pts_curr, None)

	# Select the best 50% of the points
	fb_err = np.sqrt(np.power(pts_back - pts_prev, 2).sum(axis=1))
	idx=np.argsort(fb_err)
	#idx=idx[:len(idx)/2]

	img_tmp=img_prev.copy()
	for pt_p,pt_c in zip(pts_prev[idx],pts_curr[idx]):
		dist=np.sqrt((pt_p[0]-pt_c[0])*(pt_p[0]-pt_c[0])+(pt_p[1]-pt_c[1])*(pt_p[1]-pt_c[1]))
		if (dist < 0.7 or dist > 19):
			continue
		pt_pt=(int(pt_p[0]),int(pt_p[1]))
		pt_ct=(int(pt_c[0]),int(pt_c[1]))
		cv2.arrowedLine(img_tmp,pt_pt,pt_ct,(0,0,255),tipLength=0.3)
	cv2.imshow("moving of best points",img_tmp)
	'''
	# Get the displacement and scale of the points
	displacement=(pts_curr[idx]-pts_prev[idx]).mean(axis=0)
	scale=np.median([(pts_curr[i2]-pts_curr[i1])/(pts_prev[i2]-pts_prev[i1]) for i1 in idx for i2 in idx if i1!=i2],axis=0)
	
	# Calc new bounding rectangle
	tl_prev,br_prev=rect_prev
	cr_prev=(np.array(tl_prev)+np.array(br_prev))/2
	cr_curr=cr_prev+displacement
	half_size_prev=(np.array(br_prev)-np.array(tl_prev))/2
	half_size_curr=half_size_prev*scale
	tl_curr=cr_curr-half_size_curr
	br_curr=cr_curr+half_size_curr
		
	print "Object moved from: "+str((cr_prev))+" to "+str(cr_curr)+" scaling is "+str(scale)
	'''
	return rect_prev
	return ((int(tl_curr[0]),int(tl_curr[1])),(int(br_curr[0]),int(br_curr[1])))
	


cap = cv2.VideoCapture('ergo2.mp4')
for _ in range(290):
	cap.read()
_,img1 = cap.read()  
tl,br=select(img1)
_,img2 = cap.read()
while (True):
	img_rect=img1.copy()
	cv2.rectangle(img_rect,tl,br,(255,255,0))
	cv2.imshow('tracking',img_rect)
	tl,br=track(img1,img2,(tl,br))
	img1=img2
	for _ in range(0):
		cap.read()
	_,img2=cap.read()

	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break

