import numpy as np
import cv2
import cv2.ml
import random

def track(img_prev,img_curr):
	img_prev=cv2.cvtColor(img_prev,cv2.COLOR_BGR2GRAY)
	img_curr=cv2.cvtColor(img_curr,cv2.COLOR_BGR2GRAY)
	flow=cv2.calcOpticalFlowFarneback(img_prev,img_curr,None,0.5,1,20,10,5,1.1,0)
	f1,f2=cv2.split(flow)
	cv2.imshow("f1",f1)
	cv2.imshow("f2",f2)

cap = cv2.VideoCapture('ergo2.mp4')
for _ in range(290):
	cap.read()
_,img1 = cap.read()  
_,img2 = cap.read()
while (True):
	img_rect=img1.copy()
	cv2.imshow('tracking',img_rect)
	track(img1,img2)
	img1=img2
	for _ in range(0):
		cap.read()
	_,img2=cap.read()

	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break

