import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
from utility import randomize_input as ri

cap = ri.RandomizedCapture('ergo1.mp4',0.01,0.4,True,0.001,0.05,4.0)
cap = ri.RandomizedCapture('ergo1.mp4')
cap=ri.RandomizedCapture('/home/tamasbence/Documents/video_ergo/clips/full_body/BASIC1_FRONT.avi')
cap.skip(289)

for _ in range(1800):	
	img1=cap.read()
	img1=(img1*255).astype(np.uint8)
	#blurred=cv2.GaussianBlur(img1,(1,71),0)
	blurred=cv2.boxFilter(img1,-1,(11,91))
	blurred=cv2.cvtColor(blurred,cv2.COLOR_BGR2GRAY)
	blurred=cv2.Canny(blurred,40,55,3)
	cv2.imshow("blurred",blurred)
	
	if cv2.waitKey(1) & 0xFF == ord('q'):
        	break

