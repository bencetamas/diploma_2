import sys
import datetime
from os import walk
from os import path
import os
import re
from Log import log
import cv2
import random

class ImageFinder:
	def __init__(self,dir,from_frame=0,processed_files=None):
		# Init the list of already processed file, based on the passed txt file list
		self.processed_files=[]
		proc_file_cache={}
		self.files_begun={}
		if processed_files!=None:
			with open(processed_files, "r") as ins:
    				for line in ins:
					line=line.rstrip("\n").rstrip("\r")
					split=line.split(" ")
					if len(split)==1:
						self.processed_files.append(line)
						proc_file_cache[line]=-1
        				else:
						self.files_begun[split[0]]=int(split[1])

		# Init the list of files in the directory
		self.files = []
		if path.isfile(dir):
			self.files=[dir]
		else:
			for (d, _dirnames, files) in walk(dir):
				for f in files:
					sep=os.sep
					if d[-1:]==os.sep:
						sep=""
					df=d+sep+f
					if not df in proc_file_cache and not df in self.files_begun:
		    				self.files.append(df)
    
		# Init the frame number
		self.from_frame=from_frame
		log("Init from file system done.")

		''' Debug code 
		log("Loaded file list:"+str(self.files),severity="DEBUG")
		log("Already processed file list:"+str(self.processed_files+self.files_begun.keys()),severity="DEBUG")
		#self.processed_files=["test1.txt","ImageFinder.py","Log.pyc"]
		#self.files_begun["test2.txt"]=12
		'''

		# Info about the current file
		self.currentName=None
		self.currentFrame=None # In case of image: -1
		self.cap=None
		self.im=None

	def getCurrent(self):
		return self.im

	def getCurrentName(self):
		if self.currentName==None:
			return ""
		else:
			return self.currentName

	def getCurrentFrame(self):
		return self.currentFrame

	def getCurrentFrameStr(self):
		if self.currentFrame==None or self.currentFrame < 0:
			return ""
		else:
			return str(self.currentFrame)

	def next(self,skip_frames=0):
		if self.currentFrame==None or self.currentFrame<0:
			return self.nextFile()
		for _ in range(skip_frames+1):
			ret,self.im=self.cap.read()
			self.currentFrame=self.currentFrame+1
			if not ret:		
				log("Reached end of video stream. Loading the next file!. (func: next)",severity="DEBUG")
				return self.nextFile()
		return self.im

	def nextFile(self,randomize=True,putToFullyProcessed=False):
		# Check if current file is over, update processed frames structures
		if self.currentName==None or self.currentFrame<0:
			if self.currentName!=None:
				log("Image "+str(self.currentName)+" added to processed list.",severity="DEBUG")
				self.processed_files.append(self.currentName)
		else:
			ret,_=self.cap.read()
			if ret and not putToFullyProcessed:
				log("Video "+str(self.currentName)+" added to half-processed list. (frame:"+str(self.currentFrame)+")",severity="DEBUG")
				self.files_begun[self.currentName]=self.currentFrame
			else:
				log("Video "+str(self.currentName)+" added to processed list.",severity="DEBUG")
				self.processed_files.append(self.currentName)
		self.currentName=None
		self.currentFrame=-1
		self.cap=None
		self.im=None
		from_frame=self.from_frame

		# Select new file
		if len(self.files_begun)>0:
			# Select from half processed
			nFile=self.files_begun.keys()[0]
			if randomize and len(self.files_begun)>1:
				nFile=self.files_begun.keys()[random.randint(0,len(self.files_begun.keys())-1)]
			from_frame=self.files_begun[nFile]
			del self.files_begun[nFile]
		elif len(self.files)==0:
			# No more files
			return None
		else:
			# Select from not processed files
			ind=0
			if randomize and len(self.files)>1:
				ind=random.randint(0,len(self.files)-1)
			nFile=self.files[ind]
			del self.files[ind]
		log("Choosed file "+str(nFile)+" to load for annotation.",severity="INFO")
		# Open the selected file
		self.im=cv2.imread(nFile)
		self.currentName=nFile
		if self.im==None:
			self.cap=cv2.VideoCapture(nFile)
			if self.cap.isOpened():
				ret,self.im=self.cap.read()
			if not self.cap.isOpened() or not ret:
				self.processed_files.append(nFile)
				log("Cannot open file "+str(nFile)+", trying the next one!.",severity="WARNING")
				return self.nextFile(randomize)
			self.currentName=nFile
			self.currentFrame=0
			if from_frame!=None:
				for _ in range(from_frame):
					ret,self.im=self.cap.read()
					self.currentFrame=self.currentFrame+1
					if not ret:		
						log("Cannot read next video frame. Trying the next file!.",severity="WARNING")
						return self.nextFile(randomize)
		return self.im

	def release(self,processed_files=None):
		if self.currentName!=None and self.currentFrame>=0:
			self.files_begun[self.currentName]=self.currentFrame
		if processed_files!=None:
			with open(processed_files, "w") as text_file:
				for f in self.processed_files:
		    			text_file.write(f+"\n")
				for f in self.files_begun:
					text_file.write(f+" "+str(self.files_begun[f])+"\n")

def __init__():
	random.seed()

''' Debug code 
test=ImageFinder("./testimg/",processed_files="proc.txt")
for _ in range(2):
	cv2.imshow("test",test.nextFile())
	cv2.waitKey(0)
for _ in range(4900):
	img=test.next()
	if img!=None:
		cv2.imshow("test",img)
		cv2.waitKey(10)
test.release("proc.txt")
'''
