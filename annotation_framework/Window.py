import sys
import datetime
from os import walk
from os import path
import os
import re
from Log import log
import cv2
import random
import numpy as np
import MouseEvent
import FeatureSelector
import Utility
import ImageFinder
import argparse
import Tkinter
import tkMessageBox
import json
import bson
import shutil

def mouseClbk(event, x, y, flags, param):
	if mouseClbk.obj!=None:
                if event == cv2.EVENT_LBUTTONDOWN:
                        mouseEvent=MouseEvent.MouseEvent.DOWN
                elif event == cv2.EVENT_MOUSEMOVE and (flags & cv2.EVENT_FLAG_LBUTTON) != 0:
                        mouseEvent=MouseEvent.MouseEvent.ISDOWN
                elif event == cv2.EVENT_MOUSEMOVE:
                        mouseEvent=None
                elif event == cv2.EVENT_LBUTTONUP:
                        mouseEvent=MouseEvent.MouseEvent.UP
                mouseClbk.obj.dispatchMouse(mouseEvent,(x,y))
mouseClbk.obj=None

class Window:
	def __init__(self,imageFinder,featureSelectors):
		cv2.namedWindow("main")
		cv2.setWindowProperty("main", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN);
        	cv2.setMouseCallback("main", mouseClbk)
                mouseClbk.obj=self
                self.last_mouse_coord=(0,0)

		self.zoomedIsLocked=False
                w,h=self.getResizedImagesSize()
		self.zoomCenterInWindowCoord=(w/2,h/2)
		self.imageFinder=imageFinder
                self.featureSelectors=featureSelectors
                
                self.origImg=self.imageFinder.next()
                if self.origImg==None:
                        log("No image/video found!",severity="WARNING")
                        raise Utility.MyException("No image/video found!")
                self.featureSelectors.resetState(self.origImg)

        def getTopLeftInWindowCoord(self):
                wr,hr=self.getResizedImagesSize()
                tl=(int(self.zoomCenterInWindowCoord[0]-float(wr)/self.getZoom()/2),int(self.zoomCenterInWindowCoord[1]-float(hr)/self.getZoom()/2))
                return tl

        def getBottomRightInWindowCoord(self):
                wr,hr=self.getResizedImagesSize()
                br=(int(self.zoomCenterInWindowCoord[0]+float(wr)/self.getZoom()/2),int(self.zoomCenterInWindowCoord[1]+float(hr)/self.getZoom()/2))
                return br

        def windowCoordToOriginalImageCoord(self,(x,y)):
                wr,hr=self.getResizedImagesSize()
                if x>0 and y>0 and x<=wr and y<=hr:
                        xratio=x/float(wr-1)
                        yratio=y/float(hr-1)
                        xnew=int(xratio*self.origImg.shape[1])
                        ynew=int(yratio*self.origImg.shape[0])

                        if not self.zoomedIsLocked:
                                zx=x
                                zy=y
                                zx=min(max(zx,wr/self.getZoom()/2),wr-wr/self.getZoom()/2)
                                zy=min(max(zy,hr/self.getZoom()/2),hr-hr/self.getZoom()/2)
                                self.zoomCenterInWindowCoord=(zx,zy)
                        return (xnew,ynew)
                elif x>0 and y>0 and x<=2*wr and y<=hr:
                        # Normal displayed image coordinat space: rect borders:
                        tl=self.getTopLeftInWindowCoord()
                        br=self.getBottomRightInWindowCoord()
                        x=x-wr
                        xratio=x/float(wr-1)
                        yratio=y/float(hr-1)
                        xnew=int(xratio*self.origImg.shape[1]*(br[0]-tl[0])/wr+tl[0]*self.origImg.shape[1]/float(wr))
                        ynew=int(yratio*self.origImg.shape[0]*(br[1]-tl[1])/hr+tl[1]*self.origImg.shape[0]/float(hr))
                        return (xnew,ynew)
                return None

        def getWindowSize(self):
                return (1300,700)

        def getResizedImagesSize(self):
                w,h=self.getWindowSize()
                return (w/2,h*3/4)

	def getLargeImage(self,size):
                return self.featureSelectors.getCurrent().drawToImage(self.origImg,resizeTo=size)

	def getZoomedImage(self,size):
                M=np.array([[self.getZoom(),0.0,int(-self.getTopLeftInWindowCoord()[0]*float(self.getZoom()))],[0.0,self.getZoom(),int(-self.getTopLeftInWindowCoord()[1]*float(self.getZoom()))]]).astype(np.float32)
                return self.featureSelectors.getCurrent().drawToImage(self.origImg,resizeTo=size,trMat=M)

        def getLockStatusStr(self):
                if self.zoomedIsLocked:
                        return "active"
                else:
                        return "inactive"

        def getZoom(self):
                return self.featureSelectors.getZoom()
	
	def getStatusBar(self,(w,h)):
                bar=np.zeros((h,w,3),dtype=np.uint8)
                font = cv2.FONT_HERSHEY_SIMPLEX
                scale=0.5
                mC=self.windowCoordToOriginalImageCoord(self.last_mouse_coord)
                if mC==None:
                        mCStr=""
                else:
                        mCStr=str(mC)
        
                first_line="file:"+self.imageFinder.getCurrentName()
                first_line=first_line+"    , frame:"+self.imageFinder.getCurrentFrameStr()
                first_line=first_line+"                                , coord:"+mCStr
		if self.origImg!=None:
	                first_line=first_line+"                            , image resolution:("+str(self.origImg.shape[1])+","+str(self.origImg.shape[0])+")"
                second_line="align:"+self.featureSelectors.getAlignStatusStr()
                second_line=second_line+"       "
                second_line=second_line+"zoom:"+self.featureSelectors.getZoomStr()
                second_line=second_line+"       "
                second_line=second_line+"lock:"+self.getLockStatusStr()
		if self.featureSelectors.getCurrent().hasBrush():
			second_line=second_line+"       "
                	second_line=second_line+"brush size:"+'{:3d}'.format(self.featureSelectors.getCurrent().getBrushSize())
                third_line=""
                fourth_line="Help: lock/unlock: l, zoom: +/-, edge-align on/off:a, brush-size +/-:b/v, skip frame:s, next feature:n, undo:u, "
                fifth_line="more-times-feature:p, skip more frames:y+num+y, next video:x, quit:q."
		if not self.featureSelectors.getCurrent().isFullyDescribed() or self.featureSelectors.isNext():
			sixth_line=str(self.featureSelectors.getCurrent().getHelp())
		else:
			sixth_line="This frame is fully annoted. You can proceed with 'n' to the next one."
                cv2.putText(bar,first_line,(5,20), font, scale,(255,255,255),1,cv2.LINE_AA)
                cv2.putText(bar,second_line,(5,40), font, scale,(255,255,255),1,cv2.LINE_AA)
                cv2.putText(bar,third_line,(5,60), font, scale,(255,255,255),1,cv2.LINE_AA)
                cv2.putText(bar,fourth_line,(5,80), font, scale,(255,255,255),1,cv2.LINE_AA)
                cv2.putText(bar,fifth_line,(5,100), font, scale,(255,255,255),1,cv2.LINE_AA)
                cv2.putText(bar,sixth_line,(5,120), font, scale,(255,255,255),1,cv2.LINE_AA)
                return bar

	def getScreenImage(self):
                w,h=self.getWindowSize()
                wr,hr=self.getResizedImagesSize()
		img=np.zeros((h,w,3),dtype=np.uint8)
                img[:hr,:wr]=self.getLargeImage((wr,hr))
                img[:hr,wr:2*wr]=self.getZoomedImage((wr,hr))
                img[hr:h,:]=self.getStatusBar((w,h-hr))
                return img

        def extendAnnotationsIfPossible(self):
                if self.featureSelectors.isFullyDescribed():
                        annotation={}
                        annotation["file"]=self.imageFinder.getCurrentName()
                        annotation["frame"]=self.imageFinder.getCurrentFrame()
                        annotation["width"]=self.origImg.shape[1]
                        annotation["height"]=self.origImg.shape[0]
                        annotation["timestamp"]=str(datetime.datetime.now())
                        annotation["description"]=self.featureSelectors.getFeatureDescriptors()
                        if not annotation["file"] in self.annotations:
                                self.annotations[annotation["file"]]={}
                        self.annotations[annotation["file"]][self.imageFinder.getCurrentFrameStr()]=annotation

	# Command: the pressed keyboard key or None if not pressed
	# MouseCoord: mouse coordinates, x,y
	# MouseEvent: MouseEvent type or None
	def dispatchCommand(self,command=None):
		if command!=None and command>0:
			log("Pressed key '"+chr(command)+"'.",severity="DEBUG")
                if command==ord('l'):
                        self.zoomedIsLocked=not self.zoomedIsLocked
                elif command==ord('+'):
                        self.featureSelectors.getCurrent().increaseZoom()
                elif command==ord('-'):
                        self.featureSelectors.getCurrent().decreaseZoom()
                elif command==ord('u'):
                        self.featureSelectors.getCurrent().resetState(self.origImg)
                elif command==ord('p'):
                        if isinstance(self.featureSelectors.getCurrent(), FeatureSelector.MultiFeatureSelector):
                                self.featureSelectors.getCurrent().beginNewFeature()
                elif command==ord('a'):
                        self.featureSelectors.getCurrent().setAlignToEdges(not self.featureSelectors.getCurrent().getAlignToEdges())
                elif command==ord('s'):
                        if tkMessageBox.askokcancel("?", "Skip this frame really?"):
                                self.origImg=self.imageFinder.next()
				if self.origImg==None:
					tkMessageBox.showinfo("Closing", "No more video in the folder. The application will close. If you want to annotate more image, ask the developer. Thank you for your help!");
					return False
                                self.featureSelectors.resetState(self.origImg)
                elif command==ord('n'):
			if self.featureSelectors.isNext():
				self.featureSelectors.getNext()
                        elif self.featureSelectors.isFullyDescribed() or tkMessageBox.askokcancel("Not complete", "The annotation of this frame not complete! Are you sure, you want to skip this frame and proceed to the next one?"):
                                self.extendAnnotationsIfPossible()
                                self.origImg=self.imageFinder.next()
				if self.origImg==None:
					tkMessageBox.showinfo("Closing", "No more video in the folder. The application will close. If you want to annotate more image, ask the developer. Thank you for your help!");
					return False
                                self.featureSelectors.resetState(self.origImg)
                elif command==ord('x'):
                        if tkMessageBox.askokcancel("?", "Skip this whole video really?"):
				putToFullyProcessed=tkMessageBox.askyesno("Not containing rower?","This video doesn't contain any rowers? Can label as 'fully processed'? (If you want to annotate it later, press 'no'.))")
                                self.origImg=self.imageFinder.nextFile(putToFullyProcessed=putToFullyProcessed)
				if self.origImg==None:
					tkMessageBox.showinfo("Closing", "No more video in the folder. The application will close. If you want to annotate more image, ask the developer. Thank you for your help!");
					return False
                                self.featureSelectors.resetState(self.origImg)
                elif command==ord('y'):
                        num=0
                        c=cv2.waitKey(3000)
                        while c!=ord('y'):
                                if c>=ord('0') and c<= ord('9'):
                                        num=num*10+(c-ord('0'))
                                c=cv2.waitKey(3000)
                        if num>0 and tkMessageBox.askokcancel("?", "Skip "+str(num)+" video frame? Are you sure?"):
                                fn=self.imageFinder.getCurrentName()
                                i=0
                                while i<num and self.imageFinder.getCurrentName()==fn: 
                                        self.origImg=self.imageFinder.next()
                                        i=i+1
                                self.featureSelectors.resetState(self.origImg)
		elif command==ord('b'):
			self.featureSelectors.getCurrent().increaseBrushSize()
		elif command==ord('v'):
			self.featureSelectors.getCurrent().decreaseBrushSize()
		cv2.imshow("main",self.getScreenImage())
		return True

        def dispatchMouse(self,mouseEvent,(x,y)):
		if x>=0 and y>=0 and x<self.getWindowSize()[0] and y<self.getResizedImagesSize()[1]:
		        self.last_mouse_coord=(x,y)
		        self.featureSelectors.getCurrent().mouseAction(self.windowCoordToOriginalImageCoord(self.last_mouse_coord),mouseEvent)

class MainLoop:
        def __init__(self):
                parser = argparse.ArgumentParser(description='Annotation tool for image/video files.',epilog='For more info contact tamasbence92@gmail.com')
                parser.add_argument('-d','--dir',help="Directory, which contains the image/video files.")
                parser.add_argument('-f','--file',help="Single file to process.")
                parser.add_argument('-N','--from-frame',help="The frame number, from which the video should be processed", type=int, default=0)
                parser.add_argument('-a','--annotation-file',help="The file to which the annotation should be written out.(JSON format)", required=True, default="annotations.json")
                parser.add_argument('-p','--append-to-annotation-file',help="Instead cleaning out annotation file, append the infos to it.", default=True, type=bool)
                parser.add_argument('-r','--processed-list-file',help="File containing the infos about already processed files.")
                parser.add_argument('-s','--structure-description-file',help="File, which contains how an image should be annotated.(JSON format)",required=True,default="structure.json")
		parser.add_argument('--clean-annotation-file',dest='clean_annotation_file',help="Clear the annotation file containing the old annotations.",action="store_true")
		parser.set_defaults(clean_annotation_file=False)
                self.args=parser.parse_args()
                if self.args.dir!=None:
                        f=self.args.dir
                else:
                        f=self.args.file
                log("Starting program with the following arguments: "+str(self.args),severity="INFO")

                self.imageFinder=ImageFinder.ImageFinder(dir=f,from_frame=self.args.from_frame,processed_files=self.args.processed_list_file)
                self.featureSelectors=FeatureSelector.FeatureSelectors(description_file=self.args.structure_description_file)
                self.window=Window(self.imageFinder,self.featureSelectors)
                log("Init of application succeeded",severity="INFO")
                if os.path.isfile(self.args.annotation_file) and not self.args.clean_annotation_file:
                        with open(self.args.annotation_file,"r") as fi:  
                                self.window.annotations=json.load(fi)  
                        log("Loaded previous annotations to memory.",severity="INFO")
                else:
                        self.window.annotations={}

	def run(self):
		command=None
		i=0
		while command!=ord('q'):
			if not self.window.dispatchCommand(command):
				return
			command=cv2.waitKey(25)
			i=i+1
			if i>40*60*25:
				i=0
				tkMessageBox.showinfo("Saving to disc, please wait...","Saving to disc, please wait...")
				self.save(suffix=".backup")
				tkMessageBox.showinfo("Saving is done!","Saving is done!")
 
	def save(self,suffix=""):
                # Make a backup from the old file
                log("Saving annotations to disc...",severity="INFO")
		basename=self.args.annotation_file.split(".")[0]
		bson_name=basename+".bson"+suffix
                if os.path.isfile(self.args.annotation_file+suffix):
                        old=self.args.annotation_file+".old"+suffix
			shutil.copyfile(self.args.annotation_file+suffix,old)
		if os.path.isfile(bson_name):
			old_bson=bson_name+".old"
                        shutil.copyfile(bson_name,old_bson)
		log("Saving annotations in JSON format to "+self.args.annotation_file+suffix+" .",severity="INFO")
                with open(self.args.annotation_file+suffix,"w") as of:
                        json.dump(self.window.annotations,of,indent=4)
		log("Saving annotations in BSON format to "+bson_name+" .",severity="INFO")
		with open(bson_name,"w") as of:
			bs=bson.BSON.encode(self.window.annotations)
			of.write(str(bs))
		log("Saving succeeded.",severity="INFO")

        def release(self):
		self.imageFinder.release(self.args.processed_list_file)
                self.save()


wTk = Tkinter.Tk()
wTk.wm_withdraw()
loop=MainLoop()
#loop.run()
try:
        #pass
        loop.run()
finally:	
        loop.release()	
