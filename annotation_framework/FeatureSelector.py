import sys
import datetime
from os import walk
from os import path
import os
import re
from Log import log
import cv2
import random
import numpy as np
import MouseEvent
import Utility
import json

class FeatureSelector(object):
	def __init__(self,name,help,optional=False):
		self.zoom=4
		self.alignToEdges=True
		self.name=name
		self.help=help
		self.brushsize=-1
		self.optional=optional

	def getOptional(self):
		return self.optional

	def getZoom(self):
		return self.zoom
	
	def increaseZoom(self):
		if self.zoom<16:
			self.zoom=self.zoom*2

	def decreaseZoom(self):
		if self.zoom>2:
			self.zoom=self.zoom/2

	def hasBrush(self):
		return self.brushsize>0

	def getBrushSize(self):
		return self.brushsize

	def increaseBrushSize(self):
		if self.brushsize>0 and self.brushsize<16:
			self.brushsize=self.brushsize*2

	def decreaseBrushSize(self):
		if self.brushsize>=2:
			self.brushsize=self.brushsize/2

	def getAlignToEdges(self):
		return self.alignToEdges

	def setAlignToEdges(self,alignToEdges):
		self.alignToEdges=alignToEdges

	def getName(self):	
		return self.name

	def getHelp(self):
		return self.help

	def drawToImage(self,img,resizeTo=None,trMat=np.array([[1,0,0],[0,1,0]])):
		raise Utility.MyException("Not implemented!")

	def mouseAction(self,mouseCoordInImageSpace,mouseEvent=None):
		raise Utility.MyException("Not implemented!")

	# Reset state to handle new image
	def resetState(self,img):
		raise Utility.MyException("Not implemented!")

	def isFullyDescribed(self):
		raise Utility.MyException("Not implemented!")

	def getFeatureDescription(self):
		raise Utility.MyException("Not implemented!")

class MultiFeatureSelector(FeatureSelector):
	def __init__(self,name,help,featureSelector,atLeastOnce=True):
		super(MultiFeatureSelector, self).__init__(name,help)
		self.featureSelector=featureSelector
		self.stored_descriptions=[]
		self.atLeastOnce=atLeastOnce

	def getZoom(self):
		return self.featureSelector.getZoom()
	
	def increaseZoom(self):
		self.featureSelector.increaseZoom()

	def decreaseZoom(self):
		self.featureSelector.decreaseZoom()

	def getBrushSize(self):
		return self.featureSelector.getBrushSize()

	def increaseBrushSize(self):
		self.featureSelector.increaseBrushSize()

	def decreaseBrushSize(self):
		self.featureSelector.decreaseBrushSize()

	def getAlignToEdges(self):
		return self.featureSelector.getAlignToEdges()

	def setAlignToEdges(self,alignToEdges):
		self.featureSelector.setAlignToEdges(alignToEdges)

	def getName(self):	
		return self.name

	def getHelp(self):
		return self.help

	def drawToImage(self,img,resizeTo=None,trMat=np.array([[1,0,0],[0,1,0]])):
		return self.featureSelector.drawToImage(img,resizeTo,trMat)

	def mouseAction(self,mouseCoordInImageSpace,mouseEvent=None):
		self.featureSelector.mouseAction(mouseCoordInImageSpace,mouseEvent)

	# Reset state to handle new image
	def resetState(self,img):
		self.img=img
		self.featureSelector.resetState(img)
		self.stored_descriptions=[]

	def beginNewFeature(self):
		if self.featureSelector.isFullyDescribed():
			self.stored_descriptions.append(self.featureSelector.getFeatureDescription())
			self.featureSelector.resetState(self.img)

	def isFullyDescribed(self):
		return self.featureSelector.isFullyDescribed() or len(self.stored_descriptions)>0 or not self.atLeastOnce

	def getFeatureDescription(self):
		if self.featureSelector.isFullyDescribed():
			self.beginNewFeature()
		return self.stored_descriptions

class PointFeatureSelector(FeatureSelector):
	def __init__(self,name,help,optional=False):
		super(PointFeatureSelector, self).__init__(name,help,optional)
		self.point=None

	# img is the transformed image
	def drawToImage(self,img,resizeTo=None,trMat=np.array([[1,0,0],[0,1,0]])):
		oh, ow=img.shape[0:2]
		if resizeTo!=None:
			img=cv2.resize(img,(resizeTo[0],resizeTo[1]))
		img=cv2.warpAffine(img,trMat.astype(np.float32),(img.shape[1],img.shape[0]))
		if self.point!=None:
			p=(int(self.point[0]*resizeTo[0]/float(ow)),int(self.point[1]*resizeTo[1]/float(oh)))
			transformedCoord=Utility.transform(p,trMat,type=int)
			log("Transformed point:"+str(self.point)+" to "+str(transformedCoord)+".",severity="DEBUG")
			if transformedCoord[0]>=0 and transformedCoord[0] < img.shape[1] and transformedCoord[1]>=0 and transformedCoord[1] < img.shape[0]:
				img[transformedCoord[1],transformedCoord[0]]=(255,255,255)
				cv2.circle(img,transformedCoord,4,(0,0,255))
				cv2.circle(img,transformedCoord,8,(255,255,0))
		return img

	# mouseCoordInGlobal refers to the original image space
	def mouseAction(self,mouseCoordInImageSpace,mouseEvent=None):
		if mouseEvent==MouseEvent.MouseEvent.DOWN:
			self.point=mouseCoordInImageSpace			

	# Reset state to handle new image
	def resetState(self,img):
		self.point=None

	def isFullyDescribed(self):
		return self.point!=None

	def getFeatureDescription(self):
		if self.isFullyDescribed():
			return {"name":self.name, "type":"point", "point": self.point}
		elif self.getOptional():
			return {"name":self.name, "type":"point", "empty": "true"}
		else:
			raise Utility.MyException("Not specified feature selector fully!")

class RectangleFeatureSelector(FeatureSelector):
	def __init__(self,name,help,optional=False):
		super(RectangleFeatureSelector, self).__init__(name,help,optional)
		self.p1=None
		self.p2=None

	# img is the transformed image
	def drawToImage(self,img,resizeTo=None,trMat=np.array([[1,0,0],[0,1,0]])):
		oh, ow=img.shape[0:2]
		if resizeTo!=None:
			img=cv2.resize(img,(resizeTo[0],resizeTo[1]))
		img=cv2.warpAffine(img,trMat.astype(np.float32),(img.shape[1],img.shape[0]))
		if self.p1!=None and self.p2==None:
			p=(int(self.p1[0]*resizeTo[0]/float(ow)),int(self.p1[1]*resizeTo[1]/float(oh)))
			transformedCoord=Utility.transform(p,trMat,type=int)
			tempp=(int(self.tempp[0]*resizeTo[0]/float(ow)),int(self.tempp[1]*resizeTo[1]/float(oh)))
			tempp=Utility.transform(tempp,trMat,type=int)
			log("Transformed point:"+str(self.p1)+" to "+str(transformedCoord)+".",severity="DEBUG")
			log("Transformed point:"+str(self.tempp)+" to "+str(tempp)+".",severity="DEBUG")
			if transformedCoord[0]>=0 and transformedCoord[0] < img.shape[1] and transformedCoord[1]>=0 and transformedCoord[1] < img.shape[0]:
				img[transformedCoord[1],transformedCoord[0]]=(255,255,255)
				cv2.circle(img,transformedCoord,4,(0,0,255))
				cv2.circle(img,transformedCoord,8,(255,255,0))
				cv2.rectangle(img,transformedCoord,tempp,(255,255,255))
		elif self.p2!=None:
			p1=(int(self.p1[0]*resizeTo[0]/float(ow)),int(self.p1[1]*resizeTo[1]/float(oh)))
			p2=(int(self.p2[0]*resizeTo[0]/float(ow)),int(self.p2[1]*resizeTo[1]/float(oh)))
			tl=Utility.transform(p1,trMat,type=int)
			br=Utility.transform(p2,trMat,type=int)
			log("Transformed point:"+str(self.p1)+" to "+str(tl)+".",severity="DEBUG")
			log("Transformed point:"+str(self.p2)+" to "+str(br)+".",severity="DEBUG")
			cv2.rectangle(img,(tl[0]+2,tl[1]+2),(br[0]-2,br[1]-2),(0,0,255))
			cv2.rectangle(img,(tl[0]-2,tl[1]-2),(br[0]+2,br[1]+2),(255,255,0))
		return img

	# mouseCoordInGlobal refers to the original image space
	def mouseAction(self,mouseCoordInImageSpace,mouseEvent=None):
		if mouseEvent==MouseEvent.MouseEvent.DOWN:
			if self.p1==None:
				self.p1=mouseCoordInImageSpace	
				self.tempp=mouseCoordInImageSpace		
			else:
				self.p2=mouseCoordInImageSpace
				if self.p1[0]>self.p2[0]:
					tmp=self.p1[0]
					self.p1=(self.p2[0],self.p1[1])
					self.p2=(tmp,self.p2[1])
				if self.p1[1]>self.p2[1]:
					tmp=self.p1[1]
					self.p1=(self.p1[0],self.p2[1])
					self.p2=(self.p2[0],tmp)
		elif mouseEvent==None and self.p2==None:
			self.tempp=mouseCoordInImageSpace

	# Reset state to handle new image
	def resetState(self,img):
		self.p1=None
		self.p2=None

	def isFullyDescribed(self):
		return self.p2!=None

	def getFeatureDescription(self):
		if self.isFullyDescribed():
			return {"name":self.name, "type":"rectangle", "tl": self.p1, "br":self.p2}
		elif self.getOptional():
			return {"name":self.name, "type":"rectangle", "empty": "true"}
		else:
			raise Utility.MyException("Not specified feature selector fully!")

class LineFeatureSelector(FeatureSelector):
	def __init__(self,name,help,optional=False):
		super(LineFeatureSelector, self).__init__(name,help,optional)
		self.ps=[]
		self.drawing=False
		self.img=None
		self.edges=None
		self.closedShape=False

	# img is the transformed image
	def drawToImage(self,img,resizeTo=None,trMat=np.array([[1,0,0],[0,1,0]])):
		oh, ow=img.shape[0:2]
		if resizeTo!=None:
			img=cv2.resize(img,(resizeTo[0],resizeTo[1]))
		img=cv2.warpAffine(img,trMat.astype(np.float32),(img.shape[1],img.shape[0]))
		if self.getAlignToEdges():
			eim=self.edges
			if resizeTo!=None:
				eim=cv2.resize(eim,(resizeTo[0],resizeTo[1]))
			eim=cv2.warpAffine(eim,trMat.astype(np.float32),(eim.shape[1],eim.shape[0]))
			eim=img*0.18+eim*1.5
			eim=eim.astype(np.uint8)
			
		else:
			eim=img
		color1=(random.randint(180,255),random.randint(180,255),random.randint(180,255))
		prev_transformed_p=None
		ps=self.ps
		if self.closedShape and self.isFullyDescribed():
			ps=ps.append(ps[0])
		for p in self.ps:
			p=(int(p[0]*resizeTo[0]/float(ow)),int(p[1]*resizeTo[1]/float(oh)))
			p=Utility.transform(p,trMat,type=int)
			log("Transformed point: (?,?) to "+str(p)+".",severity="DEBUG")
		
			if prev_transformed_p==None:
				if p[0]>=0 and p[0] < eim.shape[1] and p[1]>=0 and p[1] < eim.shape[0]:
					eim[p[1],p[0]]=(255,255,255)
				cv2.circle(eim,p,4,(0,0,255))
				cv2.circle(eim,p,8,(255,255,0))	
			else:
				
				cv2.line(eim,prev_transformed_p,p,color1)
			prev_transformed_p=p
		return eim

	# mouseCoordInGlobal refers to the original image space
	def mouseAction(self,mouseCoordInImageSpace,mouseEvent=None):
		if self.getAlignToEdges():
			map=[(-j,-i) for j in range(-4,5) for i in range(-4,5)]
			c=mouseCoordInImageSpace
			maxval=-1
			maxloc=None
			for m in map:
				if c[1]+m[1]<0 or c[1]+m[1]>=self.edges.shape[0] or c[0]+m[0]<0 or c[1]+m[1]>=self.edges.shape[1]:
					continue
				if self.edges[c[1]+m[1],c[0]+m[0],0]*(1.0/(i*i+j*j))>maxval:
					maxloc=(c[0]+m[0],c[1]+m[1])
					maxval=self.edges[c[1]+m[1],c[0]+m[0],0]*(1.0/(i*i+j*j))
			mouseCoordInImageSpace=maxloc
		if mouseEvent==MouseEvent.MouseEvent.DOWN:
			self.drawing=True
			self.ps=[mouseCoordInImageSpace]
		elif mouseEvent==MouseEvent.MouseEvent.ISDOWN and self.drawing:
			if self.ps[-1]!=mouseCoordInImageSpace:
				self.ps.append(mouseCoordInImageSpace)
		elif mouseEvent==MouseEvent.MouseEvent.UP and self.drawing:
			if self.ps[-1]!=mouseCoordInImageSpace:
				self.ps.append(mouseCoordInImageSpace)
			self.drawing=False

	# Reset state to handle new image
	def resetState(self,img):
		self.ps=[]
		self.drawing=False
		self.img=img
		self.edges=img
		cv2.normalize(img,self.edges,0,255,cv2.NORM_MINMAX)
		self.edges=cv2.cvtColor(self.edges,cv2.COLOR_BGR2GRAY)
		tmp=cv2.Sobel(self.edges,-1,1,1)
		self.edges=cv2.cvtColor(tmp,cv2.COLOR_GRAY2BGR)
		tmp=cv2.GaussianBlur(self.edges,(9,9),-1)
		self.edges=tmp

	def isFullyDescribed(self):
		return len(self.ps)>1 and not self.drawing

	def getFeatureDescription(self):
		if self.isFullyDescribed():
			return {"name":self.name, "type":"line", "points": self.ps}
		elif self.getOptional():
			return {"name":self.name, "type":"line", "empty": "true"}
		else:
			raise Utility.MyException("Not specified feature selector fully!")

class SegmentFeatureSelector(LineFeatureSelector):
	def __init__(self,name,help,optional=False):
		super(SegmentFeatureSelector, self).__init__(name,help,optional)
		self.closedShape=True

	def getFeatureDescription(self):
		if self.isFullyDescribed():
			return {"name":self.name, "type":"segment", "points": self.ps}
		elif self.getOptional():
			return {"name":self.name, "type":"segment", "empty": "true"}
		else:
			raise Utility.MyException("Not specified feature selector fully!")

class DummyFeatureSelector(FeatureSelector):
	def __init__(self,name,help,optional=False):
		super(DummyFeatureSelector, self).__init__(name,help,optional)
		self.closedShape=True

	def drawToImage(self,img,resizeTo=None,trMat=np.array([[1,0,0],[0,1,0]])):
		if resizeTo!=None:
			img=cv2.resize(img,(resizeTo[0],resizeTo[1]))
		img=cv2.warpAffine(img,trMat.astype(np.float32),(img.shape[1],img.shape[0]))
		return img

	def mouseAction(self,mouseCoordInImageSpace,mouseEvent=None):
		pass

	# Reset state to handle new image
	def resetState(self,img):
		pass

	def isFullyDescribed(self):
		return True

	def getFeatureDescription(self):
		return {}

class FeatureSelectors:
	def __init__(self,description_file):
		with open(description_file,"r") as fi:
			struct=json.load(fi)
		self.features=[]
		for sel in struct["selectors"]:
			name=sel["name"]
			help=sel["help"]
			optional=sel["optional"]
			multiple=sel["multiple"]
			if sel["type"]=="point":
				f=PointFeatureSelector(name,help,optional)
				if multiple:
					f=MultiFeatureSelector(name,help,f,not optional)
			elif sel["type"]=="rectangle":
				f=RectangleFeatureSelector(name,help,optional)
				if multiple:
					f=MultiFeatureSelector(name,help,f,not optional)
			elif sel["type"]=="line":
				f=LineFeatureSelector(name,help,optional)
				if multiple:
					f=MultiFeatureSelector(name,help,f,not optional)
			elif sel["type"]=="segment":
				f=SegmentFeatureSelector(name,help,optional)
				if multiple:
					f=MultiFeatureSelector(name,help,f,not optional)
			elif sel["type"]=="dummy":
				f=DummyFeatureSelector(name,help,optional)
			self.features.append(f)

		self.ind=0

	def getAlignStatusStr(self):
		feature=self.getCurrent()
		if feature.getAlignToEdges():
			return "on "
		else:
			return "off"

	def getZoom(self):
		feature=self.getCurrent()
		return feature.getZoom()

	def getZoomStr(self):
		feature=self.getCurrent()
		return '{:3d}'.format(self.getZoom())

	def resetState(self,img):
		self.ind=0
		for f in self.features:
			f.resetState(img)

	def getCurrent(self):
		return self.features[self.ind]
		
	def isNext(self):
		return self.ind<len(self.features)-1

	def getNext(self):
		if not self.isNext():
			raise Utility.MyException("End of feature list!")
		self.ind=self.ind+1
		return self.features[self.ind]

	def isFullyDescribed(self):
		for f in self.features:
			if not f.isFullyDescribed() and not f.getOptional():
				return False
		return True

	def getFeatureDescriptors(self):
		desc={}
		for f in self.features:
			desc[f.getName()]=f.getFeatureDescription()
		return desc
