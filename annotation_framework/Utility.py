import numpy as np

class MyException():
	def __init__(self,msg="Error!"):
		self.msg=msg

	def __str__(self):
		return self.msg

# 2d vector+2x3 matrix
# or: 3d vector, 3x4 matrix
def transform(vector,matrix,type=np.float64):
	translate=matrix[:,matrix.shape[1]-1]
	rotScale=matrix[:,:matrix.shape[1]-1]
	vector=(rotScale.dot(vector)+translate).astype(type)
	if len(vector)==2:
		return (vector[0],vector[1])
	else:
		return (vector[0],vector,[1],vector[2])
