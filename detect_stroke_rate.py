import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

threshold1=40
threshold2=50
kernel_size=5
min_frames=30
min_distance=180
#min_distance=100
frames_skipped=3
frames_skipped2=6

video_start=1290
#video_start=31*30#30#22*24
#video_name='ergo1.mp4'
#video_name='ergo2.mp4'
video_name='ergo3.MOV'
#video_name="/home/tamasbence/Documents/video_ergo/raw/PETI/FRONT1.MOV"
#video_name="/home/tamasbence/Documents/video_ergo/raw/PETI/REAR3.MOV"
#video_name="/home/tamasbence/Documents/video_ergo/raw/PETI/youtube0.mp4"

def getImageDiff(img1,img2,only_pos=False,bgr=True,threshold=threshold1,blend_color_spaces=True,apply_gauss=False):
	if apply_gauss:
		img1_=cv2.GaussianBlur(img1,(3,3),-1)
		img2_=cv2.GaussianBlur(img2,(3,3),-1)
	else:
		img1_=img1
		img2_=img2
	diff=img1_+(-1.0)*img2_
	_,mask1=cv2.threshold(diff,threshold,255,cv2.THRESH_BINARY)
	_,mask2=cv2.threshold(diff,-threshold,255,cv2.THRESH_BINARY_INV)
	if only_pos:
		mask=mask1
	else:
		mask=((mask1+mask2)).astype(np.uint8)
	if blend_color_spaces:
		b,g,r=cv2.split(mask)
		mask=cv2.bitwise_or(b,cv2.bitwise_or(g,r))
	else:
		mask,_,_=cv2.split(mask)
	if bgr:
		mask=cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
	return mask

def getMaskFromFrames(frames,t):
	t1=t-frames_skipped2/frames_skipped
	if t1<0:
		t1=t+frames_skipped2/frames_skipped
	diff=frames[t]+(-1.0)*frames[t1]
	_,mask=cv2.threshold(diff,threshold2,255,cv2.THRESH_BINARY)
	b,g,r=cv2.split(mask)
	mask=b+g+r
	_,mask=cv2.threshold(mask,1,255,cv2.THRESH_BINARY)
	mask=mask.astype(np.uint8)
	kernel = np.ones((kernel_size,kernel_size),np.uint8)	
	mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
	return mask

def getAverage(frames):
	avg=frames[0].astype(np.int32)
	count=1
	for frame in frames[1:]:
		avg=avg+frame
		count=count+1
	avg=avg/count
	return avg

def drawMatches(img1,img2,bg,ratio=0.75):
	diff1=getImageDiff(img1,bg,bgr=False).astype(np.uint8)
	kernel = np.ones((6,6),np.uint8)
	diff1=cv2.morphologyEx(diff1,cv2.MORPH_OPEN,kernel)
	diff2=getImageDiff(img2,bg,bgr=False).astype(np.uint8)
	diff2=cv2.morphologyEx(diff2,cv2.MORPH_OPEN,kernel)
		
	#desc = cv2.ORB_create()
	#desc = cv2.xfeatures2d.SIFT_create()
	desc = cv2.BRISK_create()

	#kp1, des1 = desc.detectAndCompute(img1,diff1)
	#kp2, des2 = desc.detectAndCompute(img2,diff2)

	# Calc for every moving points!
	kp1=[]
	kp2=[]
	for x in range(img1.shape[1]):
		for y in range(img1.shape[0]):
			kp=cv2.KeyPoint()
			kp.pt=(x,y)
			if diff1[y,x]>0:
				kp1.append(kp)
			if diff2[y,x]>0:
				kp2.append(kp)
		
	kp1,des1 = desc.compute(img1,kp1)
	kp2,des2 = desc.compute(img2,kp2)

	#bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True) 
	bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=False)
	#matches = bf.match(des1,des2)
	matches = bf.knnMatch(des1,des2, k=2)
	good = []
	for m,n in matches:
	    	if m.distance < ratio*n.distance:
			good.append(m)
	matches=good
	matches = sorted(matches, key = lambda x:x.distance)
	draw_params = dict(matchColor = (0,255,0), # draw matches in green color
		           singlePointColor = None,
		           matchesMask = None, # draw only inliers
		           flags = 2)
	match_img = cv2.drawMatches(img1,kp1,img2,kp2,matches[:200], None, **draw_params)
	cv2.imshow("matches",match_img)

def graphCut(img,diff,probably_fg_rect=None):
	kernel=np.ones((20,20),np.uint8)
	kernel2=np.ones((13,13),np.uint8)
	fg = cv2.erode(diff, kernel)	
	bg = cv2.dilate(diff, kernel2)
	mask=np.ones(img.shape[:2],np.uint8)*cv2.GC_PR_BGD
	mask[fg==255]=1
	mask[bg==0]=0
	s=4
	mask[probably_fg_rect[1]-s:probably_fg_rect[1]+probably_fg_rect[3]+s,probably_fg_rect[0]-s:probably_fg_rect[0]+probably_fg_rect[2]+s]=cv2.GC_PR_BGD
	mask[probably_fg_rect[1]:probably_fg_rect[1]+probably_fg_rect[3],probably_fg_rect[0]:probably_fg_rect[0]+probably_fg_rect[2]]=cv2.GC_PR_FGD
	cv2.imshow("graphcutmask",mask*50)
	
	bgdModel = np.zeros((1,65),np.float64)
	fgdModel = np.zeros((1,65),np.float64)
	mask, bgdModel, fgdModel = cv2.grabCut(img,mask,None,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)
	
	mask[mask==3]=255
	mask[mask==1]=255
	cv2.imshow("graphcut segmentation1",mask)

	img=np.copy(img)
	img[mask==0]=(0,0,0)
	img[mask==2]=(0,0,0)
	#img[mask==3]=(0,0,0)
	cv2.imshow("graphcut segmentation2",img)
	

cap = cv2.VideoCapture(video_name)
for i in range(video_start):
	cap.grab()
_,img1=cap.read()
_,img2=cap.read()
#img1=cv2.flip(img1,1)
#img2=cv2.flip(img2,1)

frames=[]
t=0
min_t=-1
max_t=-1
mean_t=-1
min_x=1000
max_x=-1000
mean_x=-1000

prevmom=None
prevrect=None
maxm00=-1000
while (True):	
	mask=getImageDiff(img1,img2,bgr=False,threshold=60)

	cv2.imshow("puremask",mask)
	kernel = np.ones((kernel_size,kernel_size),np.uint8)
	mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
	mom=cv2.moments(mask)
	if mom["m00"]>0:
		if mom["m10"]/mom["m00"]>max_x:
			max_t=t
			max_x=mom["m10"]/mom["m00"]
		if mom["m10"]/mom["m00"]<min_x:
			min_t=t
			min_x=mom["m10"]/mom["m00"]
		if min_t>0 and max_t>0 and mean_t < 0 and prevmom["m00"] > 0 and prevmom["m10"]/prevmom["m00"]>mom["m10"]/mom["m00"] and mom["m10"]/mom["m00"]>min_x+(max_x-min_x)/3:
			mean_x=mom["m10"]/mom["m00"]
		elif mean_t<0 and mean_x>0 and mom["m10"]/mom["m00"]<=min_x+(max_x-min_x)/3:
			mean_t=t			
		if maxm00<mom["m00"]:
			maxm00=mom["m00"]

		print mom["m10"]/mom["m00"]

	temp=cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
	if prevmom!=None and prevrect!=None:
		if mom["m00"] > maxm00*0.5 and prevmom["m00"]>maxm00*0.5:
			#print "X avg diff: "+str(mom["m10"]/mom["m00"]-prevmom["m10"]/prevmom["m00"])
			p1=(int(prevmom["m10"]/prevmom["m00"]),int(prevmom["m01"]/prevmom["m00"]))
			p2=(int(mom["m10"]/mom["m00"]),int(mom["m01"]/mom["m00"]))
			cv2.line(temp,p1,p2,(0,0,255))
			cv2.rectangle(temp,(prevrect[0],prevrect[1]),(prevrect[0]+prevrect[2],prevrect[1]+prevrect[3]),(0,255,0))
			cv2.rectangle(temp,(cv2.boundingRect(mask)[0],cv2.boundingRect(mask)[1]),(cv2.boundingRect(mask)[0]+cv2.boundingRect(mask)[2],cv2.boundingRect(mask)[1]+cv2.boundingRect(mask)[3]),(255,255,0))
			#print "Rectangle right side movement:"+str(cv2.boundingRect(mask)[0]+cv2.boundingRect(mask)[2]-prevrect[0]-prevrect[2])
	prevmom=mom
	prevrect=cv2.boundingRect(mask)

	cv2.imshow("mask",temp)

	frames=frames+[img1]
	t=t+1
	img1=img2
	for _ in range(frames_skipped):
		cap.read()
	_,img2=cap.read()
	#img2=cv2.flip(img2,1)

	if max_x-min_x>min_distance and t > min_frames and mean_t>0:
		min_mask=getMaskFromFrames(frames,min_t)
		max_mask=getMaskFromFrames(frames,max_t)
		
		bg=np.zeros(img1.shape,dtype=np.uint8)
		rect=cv2.boundingRect(max_mask)
		bg[:,0:rect[0]]=frames[max_t][:,0:rect[0]]
		bg[:,rect[0]:bg.shape[1]]=frames[min_t][:,rect[0]:bg.shape[1]]
		min_mask[:,0:rect[0]]=np.zeros((img1.shape[0],rect[0]),dtype=np.uint8)
		legRect=cv2.boundingRect(min_mask)
		#dm=getAverage(frames[max_t-3:max_t])
		#bg[legRect[1]:legRect[1]+legRect[3],legRect[0]:legRect[0]+legRect[2]]=dm[legRect[1]:legRect[1]+legRect[3],legRect[0]:legRect[0]+legRect[2]]
		#cv2.rectangle(bg,(legRect[0],legRect[1]),(legRect[2]+legRect[0],legRect[3]+legRect[1]),(0,0,255))
		cv2.imshow("bg",bg)

		#mean=frames[mean_t]
		mean=img2
		meandiff=getImageDiff(mean,bg,bgr=False,blend_color_spaces=True,threshold=30).astype(np.uint8)
		kernel = np.ones((3,3),np.uint8)
		meandiff=cv2.dilate(meandiff,kernel)
		_, contours, _ = cv2.findContours(meandiff,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
		#meandiff=cv2.morphologyEx(meandiff,cv2.MORPH_OPEN,kernel)
		max_area=-1
		max_contour=None
		for c in contours:
			if cv2.contourArea(c)>max_area:
				max_area=cv2.contourArea(c)
				max_contour=c
		meandiff=cv2.drawContours(np.zeros(meandiff.shape,dtype=np.uint8),[max_contour],0,(255),-1)
		meandiff2=getImageDiff(mean,bg,bgr=False,blend_color_spaces=True,threshold=30).astype(np.uint8)
		meandiff2=cv2.erode(meandiff2,kernel)
		meandiff=cv2.bitwise_and(meandiff,meandiff2)

		# GraphCut
		legRect=(legRect[0],legRect[1]+legRect[3]/2,legRect[2],legRect[3]/2)
		graphCut(img2,meandiff,legRect)

		'''
		meanrect=cv2.boundingRect(meandiff)
		head=np.copy(meandiff)
		head[meanrect[1]+meanrect[3]/4:,:]=np.zeros((head.shape[0]-meanrect[1]-meanrect[3]/4,head.shape[1]),dtype=np.uint8)
		headrect=cv2.boundingRect(head)
		cv2.rectangle(mean,(headrect[0],headrect[1]),(headrect[2]+headrect[0],headrect[3]+headrect[1]),(0,0,255))
		head=mean[headrect[1]:headrect[1]+headrect[3],headrect[0]:headrect[0]+headrect[2]]	
		seat=np.copy(meandiff)
		seat[meanrect[1]:meanrect[1]+meanrect[3]*3/4,:]=np.zeros((meanrect[3]*3/4,seat.shape[1]),dtype=np.uint8)
		cv2.imshow("seat",seat)
		seatrect=cv2.boundingRect(seat)	
		#seatrect=(meanrect[0],meanrect[1]+meanrect[3]-headrect[3]/2,headrect[2],headrect[3]/2)
		seatrect=(seatrect[0],seatrect[1],headrect[2],headrect[3])
		cv2.rectangle(mean,(seatrect[0],seatrect[1]),(seatrect[2]+seatrect[0],seatrect[3]+seatrect[1]),(0,255,0))

		back_segments=10
		y_upper_bound=headrect[1]+headrect[3]
		y_lower_bound=seatrect[1]
		y_trunk_length=y_lower_bound-y_upper_bound
		y_segment_length=y_trunk_length/back_segments
		last_ps=None
		for i in range(back_segments):
			segment_up_y=y_upper_bound+(i-1)*y_segment_length
			if segment_up_y < meanrect[1]:
				segment_up_y=meanrect[1]
			segment_lo_y=y_upper_bound+i*y_segment_length
			segment=np.copy(meandiff)
			segment[meanrect[1]:segment_up_y,:]=np.zeros((segment_up_y-meanrect[1],segment.shape[1]),dtype=np.uint8)
			segment[segment_lo_y:meanrect[1]+meanrect[3],:]=np.zeros((meanrect[1]+meanrect[3]-segment_lo_y,segment.shape[1]),dtype=np.uint8)
			segmentrect=cv2.boundingRect(segment)
			if last_ps!=None:
				cv2.line(mean,last_ps,(segmentrect[0],segmentrect[1]),(255,0,255))
			last_ps=(segmentrect[0],segmentrect[1])
		

		diff=getImageDiff(img2,bg,bgr=False,threshold=60)
		kernel = np.ones((6,6),np.uint8)
		diff2=cv2.erode(diff,kernel)
		diff=cv2.merge((diff,diff2,np.zeros(diff.shape,dtype=np.uint8)))
		cv2.imshow("diff",meandiff)
		cv2.imshow("mean",mean)
		'''
		if cv2.waitKey(0) & 0xFF == ord('q'):
			break
	if cv2.waitKey(1) & 0xFF == ord('q'):
        	break
