import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

cap = cv2.VideoCapture('ergo1.mp4')
for i in range(280):
	cap.read()
frames=[]
ret1,img1 = cap.read()
sum=np.zeros(img1.shape,dtype=np.float64)
_,prev=cap.read()
prev=prev/255.0
while (True):	
	ret1,img1 = cap.read() 
	img1=img1/255.0
	sub=img1+prev*(-1.0)
	prev=img1
	frames=frames+[img1]
	sum=sum+img1
	if len(frames) > 25*6:
		sum=sum-frames[0]
		frames=frames[1:]
	cv2.imshow('frame',sum/len(frames))
	diff=sum/len(frames)-img1
	_,diff=cv2.threshold(diff,0.2,1.0,cv2.THRESH_BINARY)
	#_,diff2=cv2.threshold(diff,-0.1,1.0,cv2.THRESH_BINARY_INV)
	cv2.imshow('diff',diff)
	if cv2.waitKey(40) & 0xFF == ord('q'):
        	break
