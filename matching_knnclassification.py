

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt

refPt=[]
cropping=False

def click_and_crop(event, x, y, flags, param):
	# grab references to the global variables
	global refPt, cropping
 
	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True

	elif event == cv2.EVENT_MOUSEMOVE and (flags & cv2.EVENT_FLAG_LBUTTON) != 0:
		if len(refPt)==1:
			refPt.append((x,y))
		else:
			refPt[1]=(x,y)
 
	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False

def add_simple_color_descriptors(img,kp,des):
	tmp=[]
	for i in range(len(kp)):
		pt=(int(kp[i].pt[1]),int(kp[i].pt[0]))
		colors1=img[pt[0],pt[1]]
		roi2=img[pt[0]-1:pt[0]+1,pt[1]-1:pt[1]+1].flatten()
		colors2=(np.average(roi2[0::3]),np.average(roi2[1::3]),np.average(roi2[2::3]))
		tmp=tmp+[np.concatenate((np.concatenate((des[i],colors1)),colors2))]
	return (kp,np.array(tmp))

def select_detect_compute(img,desEx):
	global refPt
	refPt=[]

	cv2.namedWindow("training")
	cv2.setMouseCallback("training", click_and_crop)	
	while (len(refPt) < 3):
        	withRectImg=img.copy()
		if len(refPt) >= 2:
			cv2.rectangle(withRectImg,refPt[0],refPt[1],(0,0,255))
		cv2.imshow('training',withRectImg)
		cv2.waitKey(100)

	img_selected=img[refPt[0][1]:refPt[2][1],refPt[0][0]:refPt[2][0] ]
	h, w, _=img.shape
	mask=np.zeros((h,w,1), dtype=np.uint8)
	cv2.rectangle(mask,refPt[0],refPt[2],(255),cv2.FILLED)
	invMask=255-mask
	
	#surf= cv2.xfeatures2d.SURF_create()
	posKp=desEx.detect(img,mask)
	_,posDes=desEx.compute(img,posKp)
	negKp=desEx.detect(img,invMask)
	_,negDes=desEx.compute(img,negKp)

	return (posKp, posDes, negKp, negDes)

cap = cv2.VideoCapture('test1.MPG')

ret1,img1 = cap.read()  
#img1=cv2.resize(img1,(0,0),fx=0.5,fy=1.0) 
 
# Initiate ORB detector
orb = cv2.ORB_create()
orb = cv2.xfeatures2d.SIFT_create()
#orb = cv2.BRISK_create()
#orb = cv2.xfeatures2d.FREAK_create()
 
# find the keypoints and descriptors with ORB
kp1, des1, negKp1, negDes1=select_detect_compute(img1,orb)
for i in range(1,0):
	ret1,img1 = cap.read()  
	kp1, tmpDes1, negKp1, tmpNegDes1=select_detect_compute(img1,orb)
	if tmpDes1.size > 0 and tmpNegDes1.size > 0:
		des1=np.concatenate((des1,tmpDes1))
		negDes1=np.concatenate((negDes1,tmpNegDes1))

#trainingData=np.concatenate((des1,negDes1))
trainingData=des1
trainingData=trainingData.astype(np.float32)
trainingLabels=np.concatenate((np.ones((len(des1)), dtype=np.int32),np.zeros((len(negDes1)), dtype=np.int32)))
trainingLabels=np.ones((len(des1)), dtype=np.int32)
# drawing keypoints
svm_img=img1.copy()
for kp in kp1:
	pt=(int(kp.pt[0]),int(kp.pt[1]))
	cv2.circle(svm_img,pt,6,(0,0,255),cv2.FILLED)
for kp in negKp1:
	pt=(int(kp.pt[0]),int(kp.pt[1]))
	cv2.circle(svm_img,pt,4,(255,0,0))
cv2.imshow('frame2',svm_img)

# KNN
knn = cv2.ml.KNearest_create()
knn.setDefaultK(1)
knn.train(trainingData,cv2.ml.ROW_SAMPLE,trainingLabels)

# print SVM accuracy on training data
good_predict= trainingLabels==([i for [i] in knn.predict(trainingData)[1]])
print trainingData.shape
print "Training count:"+str(len(good_predict))
print "Positive/negative:"+str(des1.shape[0])+"/"+str(negDes1.shape[0])
print "TP="+str(np.count_nonzero(good_predict[:des1.shape[0]]))
print "FN="+str(des1.shape[0]-np.count_nonzero(good_predict[:des1.shape[0]]))
print "TN="+str(np.count_nonzero(good_predict[des1.shape[0]:]))
print "FP="+str(negDes1.shape[0]-np.count_nonzero(good_predict[des1.shape[0]:]))
print "Accuracy on training data:"+str(np.count_nonzero(good_predict)*100/good_predict.size)

while (True):
	ret2,img2 = cap.read() 
	#img2=cv2.resize(img2,(0,0),fx=0.5,fy=1.0) 
 	#surf= cv2.xfeatures2d.SURF_create()
        kp2=orb.detect(img2,None)
	_, des2 = orb.compute(img2,kp2)
	des2=des2.astype(np.float32)
	labels2=knn.predict(des2)
	
	#_,_,dist
	dist=knn.findNearest(des2,1,np.zeros(des2.shape[0],dtype=np.float32),np.zeros(des2.shape[0],dtype=np.float32))
	dist=dist[3]
	dist=[(d,i) for d,i in zip(dist,range(len(dist)))]

	svm_img=img2.copy()
	'''
        for [label],kp in zip(labels2[1],kp2):
		pt=(int(kp.pt[0]),int(kp.pt[1]))
		if label==1:
			cv2.circle(svm_img,pt,3,(0,0,255),cv2.FILLED)
		else:
			cv2.circle(svm_img,pt,2,(255,0,0))
	'''
        for d,i in dist:
		pt=(int(kp2[i].pt[0]),int(kp2[i].pt[1]))
		if d < 80000:
			cv2.circle(svm_img,pt,3,(0,0,255),cv2.FILLED)
		else:
			cv2.circle(svm_img,pt,3,(255,0,105))
	cv2.imshow('frame',svm_img)
	
	'''
	# create BFMatcher object
	#bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
	bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)
 
	# Match descriptors.
	matches = bf.match(des1,des2)
	# Sort them in the order of their distance.
	matches = sorted(matches, key = lambda x:x.distance)

	# Draw first 10 matches.
	h,w=img1.shape[:2]
	img3=img1.copy()
	img4= cv2.drawMatches(img1,kp1,img2,kp2,matches[:10], flags=2, outImg=None)
        ''''''
	for match in matches[:100]:
        	cv2.line(img2,(int(kp1[match.queryIdx].pt[0]),int(kp1[match.queryIdx].pt[1])),(int(kp2[match.trainIdx].pt[0]),int(kp2[match.trainIdx].pt[1])),(255,255,0),1)
        
	#cv2.imshow('frame',img3)
	cv2.imshow('frame',img4)
	'''
	if cv2.waitKey(0) & 0xFF == ord('q'):
        	break

'''
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    img = cv2.imread('index.jpeg',0)
    # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
'''



