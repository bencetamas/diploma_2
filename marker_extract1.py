

import numpy as np
import cv2
import cv2.ml
import matplotlib.pyplot as plt
import random

default_interval=20
cs=[]
color1=(120,120,120)
color2=(140,140,140)
img1=None
update=False

def updateTrackbar():
	global color1, color2, img1, default_interval, update
	update=True
	cv2.setTrackbarPos('R1','image',color1[2])
	cv2.setTrackbarPos('G1','image',color1[1])
	cv2.setTrackbarPos('B1','image',color1[0])
	cv2.setTrackbarPos('R2','image',color2[2])
	cv2.setTrackbarPos('G2','image',color2[1])
	cv2.setTrackbarPos('B2','image',color2[0])
	cv2.setTrackbarPos('interval','image',default_interval)
	update=False

def trackbarHandler(_):
	global color1, color2, img1, default_interval,update
	if not update:
		color1 = (cv2.getTrackbarPos('B1','image'),cv2.getTrackbarPos('G1','image'),cv2.getTrackbarPos('R1','image'))
		color2 = (cv2.getTrackbarPos('B2','image'),cv2.getTrackbarPos('G2','image'),cv2.getTrackbarPos('R2','image'))
		default_interval=cv2.getTrackbarPos('interval','image')
		updateTrackbar()
		if (cs!=[]):
			cs[len(cs)-1]=(color1,color2)

def mouseHandler(event, x, y, flags, param):
	global color1, color2, img1, default_interval, cs
	if event == cv2.EVENT_LBUTTONDOWN or event == cv2.EVENT_MBUTTONDOWN:
		if event == cv2.EVENT_LBUTTONDOWN:
			cs=[]
		color=img1[y,x]
		color1=(max(color[0]-default_interval,0),max(color[1]-default_interval,0),max(color[2]-default_interval,0))
		color2=(min(color[0]+default_interval,255),min(color[1]+default_interval,255),min(color[2]+default_interval,255))
	if event == cv2.EVENT_MBUTTONDOWN:
		cs.append((color1,color2))
	updateTrackbar()


def getThrImg(img):
	thr=np.zeros(img1.shape[:2],np.uint8)
	if (cs==[]):
		b,g,r=cv2.split(img1)
		_,b1=cv2.threshold(b,color1[0],255,cv2.THRESH_BINARY)
		_,b2=cv2.threshold(b,color2[0],255,cv2.THRESH_BINARY_INV)
		b=cv2.bitwise_and(b1,b2)
		_,g1=cv2.threshold(g,color1[1],255,cv2.THRESH_BINARY)
		_,g2=cv2.threshold(g,color2[1],255,cv2.THRESH_BINARY_INV)
		g=cv2.bitwise_and(g1,g2)
		_,r1=cv2.threshold(r,color1[2],255,cv2.THRESH_BINARY)
		_,r2=cv2.threshold(r,color2[2],255,cv2.THRESH_BINARY_INV)
		r=cv2.bitwise_and(r1,r2)
		thr=cv2.bitwise_and(b,cv2.bitwise_and(g,r))
	else:
		for (c1,c2) in cs:
			b,g,r=cv2.split(img1)
			_,b1=cv2.threshold(b,c1[0],255,cv2.THRESH_BINARY)
			_,b2=cv2.threshold(b,c2[0],255,cv2.THRESH_BINARY_INV)
			b=cv2.bitwise_and(b1,b2)
			_,g1=cv2.threshold(g,c1[1],255,cv2.THRESH_BINARY)
			_,g2=cv2.threshold(g,c2[1],255,cv2.THRESH_BINARY_INV)
			g=cv2.bitwise_and(g1,g2)
			_,r1=cv2.threshold(r,c1[2],255,cv2.THRESH_BINARY)
			_,r2=cv2.threshold(r,c2[2],255,cv2.THRESH_BINARY_INV)
			r=cv2.bitwise_and(r1,r2)
			thr_tmp=cv2.bitwise_and(b,cv2.bitwise_and(g,r))
			thr=cv2.bitwise_or(thr,thr_tmp)
	return thr

refPt=[]
cropping=False

def click_and_crop(event, x, y, flags, param):
	# grab references to the global variables
	global refPt, cropping
 
	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True

	elif event == cv2.EVENT_MOUSEMOVE and (flags & cv2.EVENT_FLAG_LBUTTON) != 0:
		if len(refPt)==1:
			refPt.append((x,y))
		else:
			refPt[1]=(x,y)
 
	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False

def select(img):
	#return [(0,0),(0,0)]
	global refPt
	refPt=[]

	cv2.namedWindow("training")
	cv2.setMouseCallback("training", click_and_crop)	
	while (len(refPt) < 3):
        	withRectImg=img.copy()
		if len(refPt) >= 2:
			cv2.rectangle(withRectImg,refPt[0],refPt[1],(0,0,255))
		cv2.imshow('training',withRectImg)
		cv2.waitKey(100)

	return (refPt[0],refPt[2])

def select_points(rect,grid_size=1):

	tl,br=rect
	pts=[]
	for x in range(tl[0],br[0],grid_size):
		for y in range(tl[1],br[1],grid_size):
			pts=pts+[(x,y)]
	return np.array(pts).astype(dtype=np.float32)

def track(img_prev,img_curr,rect_prev):
	#rect_prev=((0,0),(img_prev.shape[1],img_prev.shape[0]))
	# Get the points to track
	pts_prev=select_points(rect_prev)	
	'''
	# params for ShiTomasi corner detection
	mask=np.zeros(img_prev.shape[:2],dtype=np.uint8)
	img_gray=cv2.cvtColor(img_prev,cv2.COLOR_BGR2GRAY)
	rtl,rbr=rect_prev
	cv2.rectangle(mask,rtl,rbr,(255),cv2.FILLED)
	'''
	'''
	# Feature descriptor method
	des=cv2.xfeatures2d.SIFT_create()
	des=cv2.ORB_create()
	kps=des.detect(img_prev,mask)
	pts_prev=np.array([k.pt for k in kps]).astype(np.float32)
	'''
	'''
	# GoodFeaturesToTrack function
	pts_prev=np.array([(pt[0][0],pt[0][1]) for pt in cv2.goodFeaturesToTrack(img_gray,2000,0.01,0,mask=mask)]).astype(np.float32)
	'''
	'''
	# Edge based method
	pts_prev=[]
	dst=cv2.Canny(img_gray,100,200,3)
	for x in range(dst.shape[0]):
		for y in range(dst.shape[1]):
			if dst[x,y]==255:
				pts_prev=pts_prev+[(y,x)]
	pts_prev=np.array(pts_prev).astype(np.float32)
	'''
	'''
	# Dense method
	pts_prev=[]
	for x in range(0,img_prev.shape[0],5):
		for y in range(0,img_prev.shape[1],5):
			pts_prev=pts_prev+[(y,x)]
	pts_prev=np.array(pts_prev).astype(np.float32)
	'''
	# Do the tracking with Lucas-Kanade
	pts_curr, status, _=cv2.calcOpticalFlowPyrLK(img_prev,img_curr,pts_prev, None)
	pts_back, _, _=cv2.calcOpticalFlowPyrLK(img_curr,img_prev,pts_curr, None)

	# Select the best 50% of the points
	fb_err = np.sqrt(np.power(pts_back - pts_prev, 2).sum(axis=1))
	idx=np.argsort(fb_err)
	idx=idx[:len(idx)/2]

	img_tmp=img_prev.copy()
	for pt_p,pt_c in zip(pts_prev[idx],pts_curr[idx]):
		dist=np.sqrt((pt_p[0]-pt_c[0])*(pt_p[0]-pt_c[0])+(pt_p[1]-pt_c[1])*(pt_p[1]-pt_c[1]))
		if (dist < 0.7 or dist > 19):
			continue
		pt_pt=(int(pt_p[0]),int(pt_p[1]))
		pt_ct=(int(pt_c[0]),int(pt_c[1]))
		cv2.arrowedLine(img_tmp,pt_pt,pt_ct,(0,0,255),tipLength=0.3)
	cv2.imshow("moving of best points",img_tmp)
	
	# Get the displacement and scale of the points
	'''max_v=0.0
	for d in pts_curr[idx]-pts_prev[idx]:
		if d[0]*d[0]+d[1]*d[1]>max_v:
			max_v=d[0]*d[0]+d[1]*d[1]
	print "max"+str(max_v)
	v=[]
	for d in pts_curr[idx]-pts_prev[idx]:
		if d[0]*d[0]+d[1]*d[1]>max_v/8:
			v.append(d)'''
	displacement=(pts_curr[idx]-pts_prev[idx]).mean(axis=0)
	#displacement=np.array(v).mean(axis=0)
	scale=np.median([(pts_curr[i2]-pts_curr[i1])/(pts_prev[i2]-pts_prev[i1]) for i1 in idx for i2 in idx if i1!=i2],axis=0)
	
	# Calc new bounding rectangle
	tl_prev,br_prev=rect_prev
	cr_prev=(np.array(tl_prev)+np.array(br_prev))/2
	cr_curr=cr_prev+displacement
	half_size_prev=(np.array(br_prev)-np.array(tl_prev))/2
	half_size_curr=half_size_prev*scale

	half_size_curr=half_size_prev # No scaling!
	tl_curr=cr_curr-half_size_curr
	br_curr=cr_curr+half_size_curr
		
	print "displ:"+str(displacement)
	print "Object moved from: "+str((cr_prev))+" to "+str(cr_curr)+" scaling is "+str(scale)
	
	#return rect_prev
	return ((int(round(tl_curr[0])),int(round(tl_curr[1]))),(int(round(br_curr[0])),int(round(br_curr[1]))))

cv2.namedWindow('image')
cv2.namedWindow('orig')
cv2.createTrackbar('R1','image',0,255,trackbarHandler)
cv2.createTrackbar('G1','image',0,255,trackbarHandler)
cv2.createTrackbar('B1','image',0,255,trackbarHandler)
cv2.createTrackbar('R2','image',0,255,trackbarHandler)
cv2.createTrackbar('G2','image',0,255,trackbarHandler)
cv2.createTrackbar('B2','image',0,255,trackbarHandler)
cv2.createTrackbar('interval','image',0,100,trackbarHandler)
updateTrackbar()
cv2.setMouseCallback("image",mouseHandler)
cv2.setMouseCallback("orig",mouseHandler)

#cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/ATC_0005.MOV')
#cap=cv2.VideoCapture('ergo3.MOV')
#cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/IMG_1114.MOV')
#cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/IMG_1131.MOV')
cap=cv2.VideoCapture('/home/tamasbence/Documents/video_ergo/raw/marker/ergo13_s1.mp4')
for _ in range(0):
	cap.grab()

_,img1 = cap.read()  
#img1=cv2.resize(img1,None,fx=0.5,fy=0.5)

while (True):
	thr=getThrImg(img1)
	cv2.imshow("image",thr)
	cv2.imshow('orig',img1)
	
	c=cv2.waitKey(25)
	if c & 0xFF == ord('s'):
        	break
	elif c & 0xFF == ord('n'):
		#for i in range(5*60):
		#	cap.grab()  
                _,img1=cap.read()
		#img1=cv2.resize(img1,None,fx=0.5,fy=0.5)


tl,br=select(img1)
ct=((tl[0]+br[0])/2,(tl[1]+br[1])/2)
_,img2=cap.read()
kernel1=np.ones((3,3),np.uint8)
kernel2=np.ones((5,5),np.uint8)
last_displ=(0,0)
while (True):
	thr1=getThrImg(img1)
	thr2=getThrImg(img2)

	thr1=cv2.erode(thr1,kernel1)
	thr1=cv2.dilate(thr1,kernel2)
	_, contours1, _ = cv2.findContours(thr1,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
	min_dist=1000000
        min_contour=None
	min_ct=None
	for c in contours1:
		x1,y1,w1,h1=cv2.boundingRect(c)
		ct2=(x1+w1/2,y1+h1/2)
		dist=np.sqrt((ct[0]-ct2[0])*(ct[0]-ct2[0])+(ct[1]-ct2[1])*(ct[1]-ct2[1]))
		if dist < min_dist and dist < 30:
			min_dist=dist
			min_contour=c
			min_ct=ct2
	if not min_ct==None:
		last_displ=(min_ct[0]-ct[0],min_ct[1]-ct[1])
		ct=min_ct
	else:
		ct=(ct[0]+last_displ[0],ct[1]+last_displ[1])
		last_displ=(last_displ[0]/2,last_displ[1]/2)
		

	thr2=cv2.erode(thr2,kernel1)
	thr2=cv2.dilate(thr2,kernel2)
	_, contours2, _ = cv2.findContours(thr1,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
	min_dist=1000000
        min_contour=None
	min_ct=None
	for c in contours2:
		x1,y1,w1,h1=cv2.boundingRect(c)
		ct2=(x1+w1/2,y1+h1/2)
		dist=(ct[0]-ct2[0])*(ct[0]-ct2[0])+(ct[1]-ct2[1])*(ct[1]-ct2[1])
		if dist < min_dist:
			min_dist=dist
			min_contour=c
			min_ct=ct2

	img_rect=img1.copy()
	#cv2.rectangle(img_rect,tl,br,(255,255,0))
	cv2.circle(img_rect,ct,5,(0,0,255),-1)
	cv2.imshow("image",thr1)
	cv2.imshow('orig',img_rect)
	#tl,br=track(thr1,thr2,(tl,br))

	c=cv2.waitKey(0)
	if c & 0xFF == ord('q'):
        	break
	elif c & 0xFF == ord('n'):
		img1=img2 
                _,img2=cap.read()
